function selectAll() {
  // var rows = document.getElementById('table').rows;
  // var inputs = document.getElementsByName('check');
  var inputs = document.getElementsByTagName('input');
  for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == 'checkbox') {
      inputs[i].checked = true;
    }
  }
}

function unselectAll() {
  // var rows = document.getElementById('table').rows;
  // var inputs = document.getElementsByName('check');
  var inputs = document.getElementsByTagName('input');
  for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == 'checkbox') {
      inputs[i].checked = false;
    }
  }
}


function addRow() {

  var firstName = document.getElementById('firstName').value;
  var lastName = document.getElementById('lastName').value;
  var username = '@' + document.getElementById('username').value;


  var table = document.getElementById('table');
  var newRow = table.insertRow(table.rows.length);

  var cel1 = newRow.insertCell(0);
  var cel2 = newRow.insertCell(1);
  var cel3 = newRow.insertCell(2);
  var cel4 = newRow.insertCell(3);

  cel1.innerHTML = '<input type="checkbox" name="check">';
  cel2.innerHTML = firstName;
  cel3.innerHTML = lastName;
  cel4.innerHTML = username;

}

// document.getElementById("table").deleteRow(1);

// function deleteRow() {
//   var table = document.getElementById('table');
//   var inputs = document.getElementsByTagName('input');
//   for (var i = 0; i < inputs.length; i++) {
//     if (inputs[i].checked && inputs[i].type == 'checkbox') {
//       table.deleteRow(i + 1);
//     }
//   }
// }
function deleteRow() {

  var table = document.getElementById('table');

  var row = table.rows.length;


  var counter = 0;

  if (table.rows.length > 1) {
    for (var i = 0; i < table.rows.length; i++) {

      var chk = table.rows[i].cells[0].childNodes[0];
      if (chk.checked) {

        table.deleteRow(i);
        row--;
        i--;
        counter += 1;
      }
    }

    if (counter == 0) {
      alert("Please select the row that you want to delete.");
    }
  } else {

    alert("There are no rows being added");
  }
}


var inputs = document.getElementsByName('check');
console.log(inputs.length);
