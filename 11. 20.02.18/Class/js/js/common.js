// function test() {
//   var list = document.getElementById('list');
//   for (var i = 0; i < list.childNodes.length; i++) {
//     document.write(list.childNodes[i].nodeType + '<br>');
//   }
// }

// test();

// setTimeout(test, 4000);


var xhr = new XMLHttpRequest();
var path = 'js/lang.json';

xhr.open('GET', path, false);
xhr.send();
var res = JSON.parse(xhr.responseText);


// for (var i = 0; i < res.length; i++) {
//   document.write(res);
// }
// for (var prop in res) {
//   document.write(prop.name);
// }

// for (key in res) {
//   document.write(res[key].name + '<br>');
// }

function formValidation() {
  var login = document.getElementById('login');
  var name = document.getElementById('name');
  var surname = document.getElementById('surname');
  var email = document.getElementById('email');
  var pass1 = document.getElementById('pass1');
  var pass2 = document.getElementById('pass2');

  if (letterValidation(login)) {
    return false;
  } else if (letterValidation(name)) {
    return false;
  } else if (letterValidation(surname)) {
    return false;
  } else if (emailValidation(email)) {
    return false;
  } else if (passValidation(pass1, pass2, 7, 12)) {
    return false;
  }
}

function passValidation(pass1, pass2, mx, my) {
  var pass1_len = pass1.value.length;
  var pass2_len = pass2.value.length;

  if (pass1_len == 0 || pass1_len >= my || pass1_len < mx) {
    alert('Password 1 should not be empty / lenght be between ' + mx + ' to ' + my);
    pass1.focus();
    return false;
  } else if (pass2_len == 0 || pass2_len >= my || pass2_len < mx) {
    alert('Password 2 should not be empty / lenght be between ' + mx + ' to ' + my);
  } else if (pass1.value == pass2.value) {
    return true;
  }
}

function letterValidation(value) {
  var letters = /^[A-Za-z]+$/;
  if (value.value.match(letters)) {
    return true;
  } else {
    alert('This field must have alphabet characters only');
    value.focus();
    return false;
  }
}

function emailValidation(value) {
  var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (value.value.match(mailFormat)) {
    return true;
  } else {
    alert('You have entered an invalid email address!');
    value.focus();
    return false;
  }
}

// function showLanguage() {
//   var input = document.getElementById('lang');
//   var ul = document.getElementById("myUL");
//   if (input.focus()) {
//     ul.style.display = 'block';
//   } if (input.blur()) {
//     ul.style.display = 'none';
//   }
// }
// showLanguage();
(function() {
  var input = document.getElementById('lang');
  var ul = document.getElementById("myUL");
  input.onblur = function() {
    ul.style.display = 'none';
  };
  // var ul = document.getElementById("myUL");
  for (var key in res) {
    ul.innerHTML += '<li>' + res[key].name + '</li>';
  }

  input.onfocus = function() {
    ul.style.display = 'block';
  };
}());

function filterLang() {

  var input, filter, ul, li, a, i, j;
  input = document.getElementById('lang');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('li');

  for (i = 0; i < li.length; i++) {
    // a = li[i].getElementsByTagName("a")[0];
    if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
    // if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
    //   li[i].style.display = "";
    // } else {
    //   li[i].style.display = "none";
    // }
  }
}
var input = document.getElementById('lang').value;
input = 'sdsdssds';
// select();
