var tabBtn = document.getElementsByClassName('tab');
var tabContent = document.getElementsByClassName('tab-content');

tabBtn[0].classList.add('active');
tabContent[0].style.display = 'block';

for (var i = 1; i < tabContent.length; i++) {
  tabContent[i].style.display = 'none';
}

function tabs(e, x) {
  var i;
   for (i = 0; i < tabBtn.length; i++) {
       tabContent[i].style.display = 'none';
       tabBtn[i].classList.remove('active');
   }
   document.getElementById(x).style.display = 'block';
   e.currentTarget.classList.add('active');
}
