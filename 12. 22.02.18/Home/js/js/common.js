// function tobin(n) {
//   var s = "";
//   for (; n >= 0; n /= 2) {
//     let rem = n % 2;
//     n -= rem;
//     s = rem + s;
//     if (n == 0) {
//       break;
//     }
//   }
//   return s;
// }


// var inputs = document.getElementsByTagName('input');
// var btn = document.getElementById('btn');
//
// btn.addEventListener('click', function() {
//   for (var i = 0; i < inputs.length; i++) {
//     var input = inputs[i].value;
//     byteString(input);
//   }
// }, false);
//
//


// function tobin(n) {
//   if (n < 0 || n > 255 || n % 1 !== 0) {
//     alert(n + " введите от 1 до 255");
//   }
//   return ("000000000" + n.toString(2)).substr(-8);
// }



var num = 0;

function init() {
	num = 0;
	update(0);
}

function update(n) {
  while (n > 255) {
    n -= 256;
  }
  while (n < 0) {
    n += 256;
  }
  document.forms[0].dec.value = n + "";
  document.forms[0].bin.value = tobin(n);
  num = n;
}

function tobin(n) {
	var s = "";
	for ( ; n >= 0; n /= 2) {
		rem = n % 2;
		n -= rem;
		s = rem + s;
		if (n == 0) {
      break;
    }
	}
	return s;
}
