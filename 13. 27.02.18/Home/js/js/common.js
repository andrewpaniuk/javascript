 /*jshint esversion: 6 */
window.addEventListener('load', init, false);

let array = [];

function init() {
  let add = document.getElementsByClassName('add')[0];
  let remove = document.getElementsByClassName('delete')[0];
  let find = document.getElementsByClassName('find')[0];
  let replace = document.getElementsByClassName('replace')[0];
  add.addEventListener('click', addElement, false);
  remove.addEventListener('click', removeElement, false);
  find.addEventListener('click', findElement, false);
  replace.addEventListener('mouseover', addInput, false);
  replace.addEventListener('click', replaceElement, false);
}

function replaceElement(e) {
  let input = document.getElementById('input');
  let inputReplace = document.getElementById('inputReplace');
  let index = array.indexOf(input.value);
  if (index == -1) {
    alert('Нет такого элемента!');
    input.value = '';
  } else {
    array.splice(index, 1, inputReplace.value);
    show();
    input.value = '';
    inputReplace.value = '';
  }
}

function addInput(e) {
  let inputReplace = document.getElementById('inputReplace');
  inputReplace.style.display = 'inline-block';
}

function show() {
  let text = document.getElementsByClassName('array')[0];
  let str = array.join('","');
  text.innerHTML = '["' + str + '"]';
}

function addElement(e) {
  let input = document.getElementById('input');
  if (input.value == '') {
    alert('Введите элемент!');
  } else {
    array.push(input.value);
    show();
    input.value = '';
  }
}

function removeElement(e) {
  let input = document.getElementById('input');
  let index = array.indexOf(input.value);
  if (index == -1) {
    alert('Нет такого элемента!');
    input.value = '';
  } else {
    array.splice(index, 1);
    show();
    input.value = '';
  }
}

function findElement(e) {
  let input = document.getElementById('input');
  let arrayFound = document.getElementsByClassName('arrayFound')[0];
  let index = array.indexOf(input.value);
  if (index == -1) {
    alert('Нет такого элемента');
    input.value = '';
  } else {
    arrayFound.innerHTML = 'Index of element: ' + index + '<br>Name of index: ' + array[index];
    input.value = '';
  }
}
