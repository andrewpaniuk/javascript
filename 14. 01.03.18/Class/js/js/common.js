/*jshint esversion: 6*/
// let xhr = new XMLHttpRequest();
// console.log(xhr);
// let path = 'https://swapi.co/api/people';
// xhr.open('GET', path, true);
// xhr.onreadystatechange = function() {
//   if (xhr.readyState == 4) {
//     console.log(xhr.readyState);
//     if (xhr.status == 200) {
//       let res = JSON.parse(xhr.responseText);
//       console.log(res);
//     }
//   }
// };
// xhr.send();

window.addEventListener('load', init, false);

function init() {

  let btnPeople = document.getElementsByClassName('show')[0];
  btnPeople.addEventListener('click', function () {
    let url = 'https://swapi.co/api/people';
    request(url, show);
    base.pagination();
  }, false);

  let btnPlanets = document.getElementsByClassName('show')[1];
  btnPlanets.addEventListener('click', function () {
    let url = 'https://swapi.co/api/planets';
    request(url, show);
    base.pagination();
  }, false);

  let btnStarships = document.getElementsByClassName('show')[2];
  btnStarships.addEventListener('click', function () {
    let url = 'https://swapi.co/api/starships';
    request(url, show);
    base.pagination();
  }, false);



  var addEvantNext = document.getElementsByClassName('btnNext')[0];
  addEvantNext.addEventListener('click', function () {
    request(base.nextPage, show);
  }, false);
  var addEvantPrev = document.getElementsByClassName('btnPrev')[0];
  addEvantPrev.addEventListener('click', function () {
    request(base.prevPage, show);
  }, false);

}

let base = {
  prevPage: null,
  nextPage: null,
  count: null,
  showloader() {
    document.getElementsByClassName('loader_wrapper')[0].style.display = 'block';
    document.getElementsByClassName('content')[0].style.display = 'none';
  },
  hideloader() {
    document.getElementsByClassName('loader_wrapper')[0].style.display = 'none';
    document.getElementsByClassName('content')[0].style.display = 'block';
  },
  pagination() {

    let pagination = document.getElementsByClassName('pagination')[0];
    pagination.style.display = 'flex';
    let count = pagination.getElementsByTagName('span')[0];
    let currentPage;
    if (base.nextPage != null) {
      currentPage = base.nextPage.split('=');
      count.innerHTML = currentPage[1] - 1 + '0' + ' of ' + base.count;
    }

    if (base.nextPage == null) {
      document.getElementsByClassName('btnNext')[0].style.display = 'none';
    } else if (base.nextPage != null) {
      document.getElementsByClassName('btnNext')[0].style.display = 'block';
    }

    if (base.prevPage == null) {
      document.getElementsByClassName('btnPrev')[0].style.display = 'none';
    } else if (base.prevPage != null) {
      document.getElementsByClassName('btnPrev')[0].style.display = 'block';
    }

  }
};

function request(url, callback) {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        let res = JSON.parse(xhr.responseText);
        if (callback == show) {
          base.nextPage = res.next;
          base.prevPage = res.previous;
          base.count = res.count;
          base.hideloader();
          callback(res, url);
        } else if (callback == showModal) {
          callback(res);
        }
      }
    }
  };
  xhr.send();
  if (callback == show) {
    base.showloader();

  }
}

function showModal(data) {
  let res = data;
  console.log(res);
  let modal = document.getElementsByClassName('modal')[0];
  let modalTitle = document.getElementsByClassName('modal-title')[0];
  let modalBody = document.getElementsByClassName('modal-body')[0];
  modalTitle.innerHTML = res.name;
  modalBody.innerHTML = '';
  for (let key in res) {
    let url = res[key];
    modalBody.innerHTML += '<span>' + key.replace('_', ' ') + '</span>' + ': ' + url + '<br>';
  }
  modal.style.display = 'block';
  let close = document.getElementsByClassName('close')[0];
  let closeBtn = document.getElementsByClassName('btn-secondary')[0];
  close.addEventListener('click', function() {
    modal.style.display = 'none';
  }, false);
  closeBtn.addEventListener('click', function() {
    modal.style.display = 'none';
  }, false);
}

function show(res, url) {
  base.pagination();
  let content = document.getElementsByClassName('content')[0];

  if (url.indexOf('people') > 1) {

    content.style.display = 'block';

    if (content.hasChildNodes) {
      content.removeChild(content.childNodes[0]);
    }

    let startTable = document.createElement('table');
    content.appendChild(startTable);

    let endThead = document.createElement('thead');
    startTable.appendChild(endThead);

    let nameTD = document.createElement('th');
    nameTD.innerText = "Name";
    endThead.appendChild(nameTD);

    let heightTD = document.createElement('th');
    heightTD.innerText = "Height";
    endThead.appendChild(heightTD);

    let massTD = document.createElement('th');
    massTD.innerText = "Mass";
    endThead.appendChild(massTD);

    let hairColorTD = document.createElement('th');
    hairColorTD.innerText = "Hair color";
    endThead.appendChild(hairColorTD);

    let skinColorTD = document.createElement('th');
    skinColorTD.innerText = "Skin color";
    endThead.appendChild(skinColorTD);

    let eyeColorTD = document.createElement('th');
    eyeColorTD.innerText = "Eye color";
    endThead.appendChild(eyeColorTD);

    let birthTD = document.createElement('th');
    birthTD.innerText = "Birth day";
    endThead.appendChild(birthTD);

    let genderTD = document.createElement('th');
    genderTD.innerText = "Gender";
    endThead.appendChild(genderTD);

    let tbodyTD = document.createElement('tbody');
    endThead.parentNode.insertBefore(tbodyTD, endThead.nextSibling);


    for (let i = 0; i < res.results.length; i++) {
      let wrapperTR = document.createElement('tr');
      tbodyTD.insertBefore(wrapperTR, tbodyTD.nextSibling);

      let personNameTD = document.createElement('td');
      personNameTD.innerText = res.results[i].name;
      personNameTD.setAttribute('class', 'personName');
      personNameTD.dataset.url = res.results[i].url;
      personNameTD.addEventListener('click', function() {
        let dataUrl = personNameTD.getAttribute('data-url');
        request(dataUrl, showModal);
      }, false);
      wrapperTR.insertBefore(personNameTD, wrapperTR.nextSibling);

      let personHeightTD = document.createElement('td');
      personHeightTD.innerText = res.results[i].height;
      wrapperTR.insertBefore(personHeightTD, wrapperTR.nextSibling);

      let personMassTD = document.createElement('td');
      personMassTD.innerText = res.results[i].mass;
      wrapperTR.insertBefore(personMassTD, wrapperTR.nextSibling);

      let personHairColorTD = document.createElement('td');
      personHairColorTD.innerText = res.results[i].hair_color;
      wrapperTR.insertBefore(personHairColorTD, wrapperTR.nextSibling);

      let personSkinColorTD = document.createElement('td');
      personSkinColorTD.innerText = res.results[i].skin_color;
      wrapperTR.insertBefore(personSkinColorTD, wrapperTR.nextSibling);

      let personEyeColorTD = document.createElement('td');
      personEyeColorTD.innerText = res.results[i].eye_color;
      wrapperTR.insertBefore(personEyeColorTD, wrapperTR.nextSibling);

      let personBirthTD = document.createElement('td');
      personBirthTD.innerText = res.results[i].birth_year;
      wrapperTR.insertBefore(personBirthTD, wrapperTR.nextSibling);

      let personGenderTD = document.createElement('td');
      personGenderTD.innerText = res.results[i].gender;
      wrapperTR.insertBefore(personGenderTD, wrapperTR.nextSibling);

    }

  } else if (url.indexOf('planets') > 1) {

    content.style.display = 'block';

    if (content.hasChildNodes) {
      content.removeChild(content.childNodes[0]);
    }

    let startTable = document.createElement('table');
    content.appendChild(startTable);

    let endThead = document.createElement('thead');
    startTable.appendChild(endThead);

    let nameTD = document.createElement('th');
    nameTD.innerText = "Name";
    endThead.appendChild(nameTD);

    let rotationTD = document.createElement('th');
    rotationTD.innerText = "Rotation";
    endThead.appendChild(rotationTD);

    let orbitalTD = document.createElement('th');
    orbitalTD.innerText = "Orbital";
    endThead.appendChild(orbitalTD);

    let diameterTD = document.createElement('th');
    diameterTD.innerText = "Diametr";
    endThead.appendChild(diameterTD);

    let climateTD = document.createElement('th');
    climateTD.innerText = "Climate";
    endThead.appendChild(climateTD);

    let gravityTD = document.createElement('th');
    gravityTD.innerText = "Gravity";
    endThead.appendChild(gravityTD);

    let terrainTD = document.createElement('th');
    terrainTD.innerText = "Terrain";
    endThead.appendChild(terrainTD);

    let surfaceWaterTD = document.createElement('th');
    surfaceWaterTD.innerText = "Surface water";
    endThead.appendChild(surfaceWaterTD);

    let populationTD = document.createElement('th');
    populationTD.innerText = "Population";
    endThead.appendChild(populationTD);

    let tbodyTD = document.createElement('tbody');
    endThead.parentNode.insertBefore(tbodyTD, endThead.nextSibling);


    for (let i = 0; i < res.results.length; i++) {
      let wrapperTR = document.createElement('tr');
      tbodyTD.insertBefore(wrapperTR, tbodyTD.nextSibling);

      let planetNameTD = document.createElement('td');
      planetNameTD.innerText = res.results[i].name;
      wrapperTR.insertBefore(planetNameTD, wrapperTR.nextSibling);

      let planetRotationTD = document.createElement('td');
      planetRotationTD.innerText = res.results[i].rotation_period;
      wrapperTR.insertBefore(planetRotationTD, wrapperTR.nextSibling);

      let planetOrbitalTD = document.createElement('td');
      planetOrbitalTD.innerText = res.results[i].orbital_period;
      wrapperTR.insertBefore(planetOrbitalTD, wrapperTR.nextSibling);

      let planetDiameterTD = document.createElement('td');
      planetDiameterTD.innerText = res.results[i].diameter;
      wrapperTR.insertBefore(planetDiameterTD, wrapperTR.nextSibling);

      let planetClimateTD = document.createElement('td');
      planetClimateTD.innerText = res.results[i].climate;
      wrapperTR.insertBefore(planetClimateTD, wrapperTR.nextSibling);

      let planetGravityTD = document.createElement('td');
      planetGravityTD.innerText = res.results[i].gravity;
      wrapperTR.insertBefore(planetGravityTD, wrapperTR.nextSibling);

      let planetTerrainTD = document.createElement('td');
      planetTerrainTD.innerText = res.results[i].terrain;
      wrapperTR.insertBefore(planetTerrainTD, wrapperTR.nextSibling);

      let planetSurfaceWaterTD = document.createElement('td');
      planetSurfaceWaterTD.innerText = res.results[i].surface_water;
      wrapperTR.insertBefore(planetSurfaceWaterTD, wrapperTR.nextSibling);

      let planetPopulationTD = document.createElement('td');
      planetPopulationTD.innerText = res.results[i].population;
      wrapperTR.insertBefore(planetPopulationTD, wrapperTR.nextSibling);

    }
  } else if (url.indexOf('starships') > 1) {
    content.style.display = 'block';
    if (content.hasChildNodes) {
      content.removeChild(content.childNodes[0]);
    }

    let startTable = document.createElement('table');
    content.appendChild(startTable);

    let endThead = document.createElement('thead');
    startTable.appendChild(endThead);

    let nameTD = document.createElement('th');
    nameTD.innerText = "Name";
    endThead.appendChild(nameTD);

    let modelTD = document.createElement('th');
    modelTD.innerText = "Model";
    endThead.appendChild(modelTD);

    let manufacturerTD = document.createElement('th');
    manufacturerTD.innerText = "Manufacturer";
    endThead.appendChild(manufacturerTD);

    let costTD = document.createElement('th');
    costTD.innerText = "Cost";
    endThead.appendChild(costTD);

    let lengthTD = document.createElement('th');
    lengthTD.innerText = "Length";
    endThead.appendChild(lengthTD);

    let maxSpeedTD = document.createElement('th');
    maxSpeedTD.innerText = "Max speed";
    endThead.appendChild(maxSpeedTD);

    let crewTD = document.createElement('th');
    crewTD.innerText = "Crew";
    endThead.appendChild(crewTD);

    let passengersTD = document.createElement('th');
    passengersTD.innerText = "Passengers";
    endThead.appendChild(passengersTD);

    let tbodyTD = document.createElement('tbody');
    endThead.parentNode.insertBefore(tbodyTD, endThead.nextSibling);


    for (let i = 0; i < res.results.length; i++) {

      let wrapperTR = document.createElement('tr');
      tbodyTD.insertBefore(wrapperTR, tbodyTD.nextSibling);

      let starshipNameTD = document.createElement('td');
      starshipNameTD.innerText = res.results[i].name;
      wrapperTR.insertBefore(starshipNameTD, wrapperTR.nextSibling);

      let starshipModelTD = document.createElement('td');
      starshipModelTD.innerText = res.results[i].model;
      wrapperTR.insertBefore(starshipModelTD, wrapperTR.nextSibling);

      let starshipManufacturerTD = document.createElement('td');
      starshipManufacturerTD.innerText = res.results[i].manufacturer;
      wrapperTR.insertBefore(starshipManufacturerTD, wrapperTR.nextSibling);

      let starshipCostTD = document.createElement('td');
      starshipCostTD.innerText = res.results[i].cost_in_credits;
      wrapperTR.insertBefore(starshipCostTD, wrapperTR.nextSibling);

      let starshipLengthTD = document.createElement('td');
      starshipLengthTD.innerText = res.results[i].length;
      wrapperTR.insertBefore(starshipLengthTD, wrapperTR.nextSibling);

      let starshipMaxSpeedTD = document.createElement('td');
      starshipMaxSpeedTD.innerText = res.results[i].max_atmosphering__speed;
      wrapperTR.insertBefore(starshipMaxSpeedTD, wrapperTR.nextSibling);

      let starshipCrewTD = document.createElement('td');
      starshipCrewTD.innerText = res.results[i].crew;
      wrapperTR.insertBefore(starshipCrewTD, wrapperTR.nextSibling);

      let starshipPasengersTD = document.createElement('td');
      starshipPasengersTD.innerText = res.results[i].pasengers;
      wrapperTR.insertBefore(starshipPasengersTD, wrapperTR.nextSibling);

    }
  }
}