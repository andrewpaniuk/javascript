/*jshint esversion:6*/

let posts = document.querySelector('#char-list');
let loader = document.querySelector('#loader');
let postsBody = document.querySelector('#posts-body');
let search = document.querySelector('#search');
let mdlBody = document.querySelector('#mdlBody');
let navLink = document.querySelectorAll('.nav-link');
let modalLoader = document.querySelector('#modal-loader');
let jumb = document.querySelector('#jumb');
let more = document.querySelector('#more');

let Base = {
  mainURL: "https://swapi.co/api/",
  items: {},
  type: '',

  init() {
    let menu = document.querySelector('#menu-btns');
    for (let i = 0; i < navLink.length; i++) {
      navLink[i].addEventListener('click', Base.handler, false);
    }
    let search = document.querySelector('#search');
    search.addEventListener('input', Base.searchItem, false);
    Pagination.init();

  },

  handler(e) {
    e.preventDefault();
    let target = e.target;
    Base.type = target.getAttribute('data-route');
    console.log(Base.type);
    posts.innerHTML = '';
    jumb.style.display = 'none';
    Pagination.page = 1;
    Base.loadData();
    if (Pagination.next == null) {
      more.style.display = 'none';
    } else if (Pagination.next != null) {
      more.style.display = '';
    }
    jumb.style.display = '';
  },

  loadData(url) {
    let path = Base.mainURL + Base.type;
    let dataUrl = url ? url : path;
    Base.showLoader();
    Base.items = Base.request(dataUrl);
    Base.hideLoader();
    postsBody.classList.remove('d-none');
    let initialHeight = document.documentElement.scrollHeight;
    for (let i = 0; i < Base.items.results.length; i++) {
      Base.display(Base.items.results[i]);
      // console.log(Base.items.results[i]);
    }
    Pagination.next = Base.items.next;
    Pagination.prev = Base.items.previous;
    Pagination.items = Base.items;
    // Pagination.pageScroll(initialHeight);
    // window.addEventListener('scroll', Pagination.pageScroll(dataUrl, initialHeight), false);
    // console.log(Base.items.results);
  },

  request(url) {
    let method = 'GET';
    let path = url;
    let xhr = new XMLHttpRequest();

    xhr.open(method, path, false);
    xhr.send();
    if (xhr.readyState == 4 && xhr.status == 200) {
      return JSON.parse(xhr.responseText);
    }
  },

  display(object) {
    let classList = {
      a: 'list-group-item list-group-item-action flex-column align-items-start lore-object-info',
      div: 'd-flex w-100 justify-content-between',
      h5: 'mb-1'
    };
    let a_elem = document.createElement('a');
    let div_elem = document.createElement('div');
    let h_elem = document.createElement('h5');

    a_elem.className = classList['a'];
    div_elem.className = classList['div'];
    h_elem.className = classList['h5'];

    posts.appendChild(a_elem);
    a_elem.appendChild(div_elem);
    div_elem.appendChild(h_elem);

    // a_elem.addEventListener('click', Base.callInfo, false);

    function Combine() { /*Combines data from API with HTML*/
      for (let i = 0; i < a_elem.childNodes.length; i++) {
        a_elem.innerHTML = '<strong>' + object.name + '</strong> ' + object.url;
        a_elem.setAttribute('data-loreobj', object.url);
        // a_elem.addEventListener('click', Base.callInfo, false);
      }
    }
    Combine();
    Base.initModal();

  },

  initModal() {
    let objectInfo = document.querySelectorAll('.lore-object-info');
    for (let i = 0; i < objectInfo.length; i++) {
      objectInfo[i].addEventListener('click', Base.callInfo, false);
    }
  },

  callInfo(e) {

    let personInfo = Base.request(e.target.getAttribute('data-loreobj'));
    $('#ModalLong').modal('show');
    Base.showModalLoader();
    Base.modalContent(personInfo);
    Base.hideModalLoader();

  },

  modalContent(data) {
    let res = '';
    mdlBody.innerHTML = '';
    for (let key in data) {
      let value = data[key];
      console.log(value);
      let links = [];
      let title = '';
      if (key == 'homeworld') {
        title = Base.getTitle(value);
        res = Base.modalLink(value, title);
      } else if (key == 'films' || key == 'starships' || key == 'vehicles' || key == 'pilots' || key == 'residents' || key == 'planets' || key == 'characters' || key == 'people' || key == 'species') {

        for (var i = 0; i < value.length; i++) {
          title = Base.getTitle(value[i]);
          console.log(title);
          links.push(Base.modalLink(value[i], title));
        }
        res = links.join(', ');
      } else {
        res = data[key];
      }

      mdlBody.innerHTML += '<p>' + key.replace('_', ' ') + ': <strong>' + res + '</strong></p>';
      Base.initModal();
    }
  },
  getTitle: function (linkItem) {

    var data = Base.request(linkItem);
    console.log(data);
    // return Object.keys(data)[0];
    // return object[Object.keys(data)[0]];
    for (let key in data) {
      return data[key];
    }
  },

  modalLink: function (url, title) {
    return '<a class="lore-object-info" href="#" data-loreobj="' + url + '">' + title + '</a>';
  },

  showLoader() {
    loader.style.display = 'block';
  },
  hideLoader() {
    loader.style.display = '';
  },

  showModalLoader() {
    mdlBody.innerHTML = '';
    modalLoader.style.display = 'block';
  },

  hideModalLoader() {
    modalLoader.style.display = '';
  },

};

// let Pagination = {
//   pages: 0,
//   next: null,
//   prev: null,
//   page: 1,
//   pageScroll(initialHeight) {
//     let count = Base.items.count;
//     this.pages = Math.ceil(count / 10);
//     let height = document.documentElement.scrollHeight - initialHeight;
//     let scrolled = document.documentElement.scrollTop;

//     console.log(height + ' ' + scrolled);
//     // for (let i = 1; i < Pagination.pages; i++) {
//     //   Pagination.page++;
//     //   Pagination.getPage(Pagination.page);
//     //   console.log(Pagination.page);

//     // }
//     if (scrolled === height) {
//       Base.showLoader();
//       Pagination.page++;
//       Pagination.getPage(Pagination.page);
//       Base.hideLoader();
//     }


//   },
//   getPage(n) {
//     Pagination.page = n;
//     if (Pagination.page < 1) {
//       Pagination.page = 1;
//     } else if (Pagination.page > Pagination.pages) {
//       Pagination.page = Pagination.pages;
//     }
//     let url = Base.mainURL + Base.type + '/?page=' + Pagination.page;
//     let dataUrl = Base.request(url);
//     for (let i = 0; i < dataUrl.results.length; i++) {
//       Base.display(dataUrl.results[i]);
//     }
//     console.log(url);
//     if (dataUrl.next == null) {
//       window.removeEventListener('scroll', function () {
//         Pagination.pageScroll(initialHeight);
//       }, false);
//     }
//     console.log(dataUrl);
//     Pagination.items = dataUrl;
//   },

//   init() {
//     let initialHeight = document.documentElement.scrollHeight;
//     if (Base.items.next !== null) {
//       window.addEventListener('scroll', function () {
//         Pagination.pageScroll(initialHeight);
//         console.log(Base.items.next);
//       }, false);
//     } else if (Base.items.next === null) {
//       window.removeEventListener('scroll', function () {
//         Pagination.pageScroll(initialHeight);
//       }, false);
//     }
//   }

// };

let Pagination = {
  pages: 0,
  next: null,
  page: 1,
  items: {},
  getPage(n) {
    Pagination.page = n;
    let url = Base.mainURL + Base.type + '/?page=' + Pagination.page;
    let dataUrl = Base.request(url);
    console.log(url);
    Base.items = dataUrl;
    for (let i = 0; i < dataUrl.results.length; i++) {
      Base.display(dataUrl.results[i]);
    }
    Pagination.next = Base.items.next;

    if (Pagination.next == null) {
      more.style.display = 'none';
    } else if (Pagination.next != null) {
      more.style.display = '';
    }

  },

  init() {
    more.addEventListener('click', function (e) {
      Pagination.page++;
      Pagination.getPage(Pagination.page);
    }, false);
  }


};

window.addEventListener('load', Base.init, false);













































// searchItem: function (e) {
//   e.preventDefault();
//   mdlBody.innerHTML = '';
//   var value = e.target.value;
//   var data;
//   var result = [];

//   data = Base.items.results;
//   for (var i = 0; i < data.length; i++) {
//     if (Base.findSubString(value, data[i].name)) {
//       result.push(data[i]);
//     }
//   }

//   console.log(result);

//   if (result.length == 0) {
//     alert('По заданим критеріям пошуку не знайдено співпадінь!');
//   } else {
//     Base.display(result);
//   }
// },

// findSubString: function (find, str) {
//   str = str.toLowerCase();
//   find = find.toLowerCase();

//   var pos = str.indexOf(find) + 1;
//   if (pos > 0) return true;

//   return false;
// },