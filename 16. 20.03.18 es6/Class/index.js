/*jshint esversion:6*/
// const COUNT = 10;
// const HELLO = 10;
// document.write(COUNT + "<br>");

// const ARR = ['Hello', 123, 345];

// ARR[ARR.length] = 1;
// ARR[ARR.length] = 12000;
// ARR.shift();
// ARR.push('Last');

// const ARR2 = {
//   name: 'NAME'
// };

// ARR2.surname = 'SURNAME';
// ARR.name = 1; // false

// alert('Hello ${COUNT}');

// alert(ARR2.name);

// for (var i = 0; i < 5; i++) {
//   setInterval(function() {
//     console.log(i);
//   }, 3000);
// }

// for (let i = 0; i < 5; i++) {
//   setInterval(function() {
//     console.log(i);
//   }, 3000);
// }


// let apples = 5;
// if (true) {
//   let apples = 10;
//   alert(apples);
// }
// alert(apples);


function setCookies() {
  // document.cookie = 'password=qwerty123';
  document.cookie = "userName=Vasya";
  // alert('2');
}

function getCookies() {
  alert(document.cookie);
}

let setBTN = document.getElementsByClassName('setCookies')[0];
let getBTN = document.getElementsByClassName('getCookies')[0];

setBTN.addEventListener('click', setCookies, false);
getBTN.addEventListener('click', getCookies, false);


window.addEventListener('load', init, false);

// function init() {
//   let id1 = document.getElementById('id1');
//   let id2 = document.getElementById('id2');
//   let div = document.getElementById('div');
//   id1.addEventListener('click', function() {
//     window.sessionStorage.setItem('key', 'value1');
//     window.sessionStorage.key2 = 'key2';
//     window.sessionStorage['key3'] = 'key3';
//   }, false);
//   id2.addEventListener('click', function() {
//     div.innerHTML = window.sessionStorage.getItem('key2');
//     console.log(window.sessionStorage.getItem('key2'));
//   }, false);
// }
function init() {
  let id1 = document.getElementById('id1');
  let id2 = document.getElementById('id2');
  let div = document.getElementById('div');
  id1.addEventListener('click', function() {
    window.localStorage.setItem('key', 'value1');
    window.localStorage.key2 = 'key2';
    window.localStorage['key3'] = 'key3';
  }, false);
  id2.addEventListener('click', function() {
    // div.innerHTML = window.localStorage.getItem('key2');
    // div.innerHTML = window.localStorage.length;
    for (let i = 0; i < window.localStorage.length; i++) {
      div.innerHTML += window.localStorage.key(i) + ': ' + window.localStorage.getItem(localStorage.key(i)) + '<br>';
    }
    // console.log(window.localStorage.getItem('key2'));
  }, false);

}