/*jshint esversion:6*/

let time = {
  check(e) {
    let el = e.target;
    let value;
    if (el.checked) {
      value = el.value;
    }
    let type = el.type;
    // console.log(e.target);
    time.setLocalStorage(type, value);
    // alert(type);
    // alert(value);
  },

  setLocalStorage(key, value) {
    window.localStorage.setItem(key, value);
    // alert(localStorage.getItem(localStorage.key(0)));
    time.changeBG();
  },

  changeBG() {
    // let localStorageValue = window.localStorage.getItem;
    // alert(localStorageValue);
    let radioInputs = document.getElementsByName('time');
    if (window.localStorage.getItem('radio') == 'day') {
      document.body.style.backgroundColor = 'yellow';
      radioInputs[0].checked = true;
    } else if (window.localStorage.getItem('radio') == 'night') {
      document.body.style.backgroundColor = 'black';
      radioInputs[1].checked = true;
    } else {
      document.body.style.backgroundColor = 'yellow';
      radioInputs[0].checked = true;
    }

  }
};

window.addEventListener('load', function () {
  let radioInputs = document.getElementsByName('time');
  time.changeBG();
  for (let i = 0; i < radioInputs.length; i++) {
    radioInputs[i].addEventListener('change', time.check);
  }
});