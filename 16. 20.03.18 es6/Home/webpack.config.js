var webpack = require('webpack');
var path = require('path');
// var HtmlPlugin = require('html-webpack-plugin');


module.exports = {

  entry: './index.js',

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },

  plugins: [
    // new HtmlPlugin()
  ],

  watch: true,
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          // babelrc: false,
          presets: ['es2015', 'env']
        }
      }
    }]
  }

};