/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*jshint esversion:6*/
// import '../sass/main.sass';
var sass = __webpack_require__(1);

var field = document.querySelectorAll('.field')[0];
var calc = {
  number: '',
  numberList: [],
  symbol: '',
  result: '',
  count: 1,
  getNumber: function getNumber(e) {
    var number = e.target;
    // console.log(number);
    field.innerHTML = '';
    if (calc.number == '' && number.innerText == 0) {
      field.innerText = '0';
      return false;
    }
    calc.number += number.innerText;
    field.innerText = calc.number;
    console.log(calc.number);
  },
  getSymbol: function getSymbol(e) {
    var symbol = e.target;
    calc.symbol = symbol.innerText;
    calc.addNumberToList();
    calc.number = '';
    field.innerText = '0';
    console.log(calc.symbol);
    console.log(calc.numberList);
  },
  add: function add(a, b) {
    return +a + +b;
  },
  minus: function minus(a, b) {
    return +a - +b;
  },
  multi: function multi(a, b) {
    return +a * +b;
  },
  dec: function dec(a, b) {
    return +a / +b;
  },
  cancel: function cancel() {
    field.innerText = '0';
    calc.number = '';
    calc.numberList = [];
  },
  addNumberToList: function addNumberToList() {
    calc.numberList.push(calc.number);
  },
  getResult: function getResult() {
    calc.addNumberToList();
    switch (calc.symbol) {
      case '+':
        calc.result = calc.add(calc.numberList[0], calc.numberList[1]);
        break;
      case '-':
        calc.result = calc.minus(calc.numberList[0], calc.numberList[1]);
        break;
      case '*':
        calc.result = calc.multi(calc.numberList[0], calc.numberList[1]);
        break;
      case '/':
        calc.result = calc.dec(calc.numberList[0], calc.numberList[1]);
        break;
      case 'C':
        calc.cancel();
        break;
      default:
        break;
    }
    field.innerText = calc.result;
    calc.number = calc.result;
    calc.numberList = [];
  },
  init: function init() {
    var numbers = document.querySelectorAll('.number');
    numbers.forEach(function (value) {
      value.addEventListener('click', calc.getNumber);
    });

    var symbols = document.querySelectorAll('.symbol');
    symbols.forEach(function (value) {
      value.addEventListener('click', calc.getSymbol);
    });

    var res = document.querySelector('#res');
    res.addEventListener('click', calc.getResult);

    var cancel = document.querySelector('#cancel');
    cancel.addEventListener('click', calc.cancel);
  }
};

window.addEventListener('load', calc.init);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map