/*jshint esversion:6*/

//FUNCTION EXPRESSION
// const SUM = function(first, second) {
//   return first + second;
// };

//ARROW FUNCTION
const SUM = (first, second) => first + second;
const FUN1 = a => a * 2;
const FUN2 = () => 'hello';
console.log(typeof FUN1);
// const SUM = (first, second) => {
  // return first + second;
// };


console.log(SUM(2, 2));

let arr = [1, 2, 3, 4, 5];
let res = 0;
for (let i = 0; i < arr.length; i++) {
  document.body.innerHTML += arr[i] + '<br>';
  res += arr[i]; 
}
console.log(res);
console.log(`${arr}`);
let sum = 0;
arr.forEach(i => sum += i);
// arr.forEach(i => { console.log(i); return sum += i; });
console.log(sum);

let obj = {
  name: 'John',
  getName() {
    setInterval(() => {
      console.log('Name: ' + this.name);
    }, 3000);
  }
};
obj.getName();
// console.log(`Hello ${obj.name}`);

