/*jshint esversion:6*/
import '../sass/main.sass';
// const sass = require('../sass/main.sass');

let field = document.querySelectorAll('.field')[0];
const calc = {
  number: '',
  numberList: [],
  symbol: '',
  result: '',
  count: 1,
  getNumber(e) {
    let number = e.target;
    // console.log(number);
    field.innerHTML = '';
    if (calc.number == '' && number.innerText == 0) {
      field.innerText = '0';
      return false;
    }
    calc.number += number.innerText;
    field.innerText = calc.number;
    console.log(calc.number);
  },
  getSymbol(e) {
    let symbol = e.target;
    calc.symbol = symbol.innerText;
    calc.addNumberToList();
    calc.number = '';
    field.innerText = '0';
    console.log(calc.symbol);
    console.log(calc.numberList);
  },
  add(a, b) {
    return (+a) + (+b);
  },
  minus(a, b) {
    return (+a) - (+b);
  },
  multi(a, b) {
    return (+a) * (+b);
  },
  dec(a, b) {
    return (+a) / (+b);
  },


  cancel() {
    field.innerText = '0';
    calc.number = '';
    calc.numberList = [];
  },

  addNumberToList() {
    calc.numberList.push(calc.number);
  },

  getResult() {
    calc.addNumberToList();
    switch (calc.symbol) {
      case '+':
        calc.result = calc.add(calc.numberList[0], calc.numberList[1]);
        break;
      case '-':
        calc.result = calc.minus(calc.numberList[0], calc.numberList[1]);
        break;
      case '*':
        calc.result = calc.multi(calc.numberList[0], calc.numberList[1]);
        break;
      case '/':
        calc.result = calc.dec(calc.numberList[0], calc.numberList[1]);
        break;
      case 'C':
        calc.cancel();
        break;
      default:
        break;
    }
    field.innerText = calc.result;
    calc.number = calc.result;
    calc.numberList = [];
  },
  init() {
    let numbers = document.querySelectorAll('.number');
    numbers.forEach(value => {
      value.addEventListener('click', calc.getNumber);
    });

    let symbols = document.querySelectorAll('.symbol');
    symbols.forEach(value => {
      value.addEventListener('click', calc.getSymbol);
    });

    let res = document.querySelector('#res');
    res.addEventListener('click', calc.getResult);

    let cancel = document.querySelector('#cancel');
    cancel.addEventListener('click', calc.cancel);
  }
};



window.addEventListener('load', calc.init);