/*jshint esversion:6*/
import '../sass/main.sass';

// const ADD_PERSON = (id, ...args) => {
//   console.log(id, args);
// };
// let arr = ['John', 'Surname', 28]
// ADD_PERSON(44, ...arr);


let arr1 = ['P', 'V', 'S'];
let arr2 = ['Pt', 'Sub'];
let result = [];
// spread
let addDays = (...args) => {
  result = [...args];
  console.log(result);
};


addDays(...arr1, 'Ch', ...arr2, 'Vos');

result.forEach(i => {
  document.write(i + '<br>');
});
// const ADD_PERSON = {
//   name: ''
// };

// ADD_PERSON.name = 'Johhny';
// console.log(ADD_PERSON.name);


const ADD_PERSON = (name, surname, age) => {
  return {
    name,
    surname,
    age,
    getHobby() {
      return 'Some hobby';
    }
  };
};

let person1 = ADD_PERSON('Johhny', 'Surname', 25);

console.log(person1.getHobby());

let obj = {
  name: 'Johnny',
  age: 25
};

// let {name} = obj;
// let {age} = obj;


// let {name:NAME} = obj;
// let {age:AGE} = obj;

let {name:NAME, age:AGE} = obj;

console.log( NAME + ' (_!_) ' + AGE );

let array = ['Stevi', 'Surname', 24];
// let [name, surname, age] = array;
let [name,, age] = array;
let [, surname,] = array;
let [,,,job = 'worker'] = array;

console.log(name, surname, age, job);
console.log(array);

let userName = 'Bill';
let userAge = 45;

// console.log('Hello, my name is ' + userName + '. Age: ' + userAge);
console.log(`Hello, my name is ${userName} Age: ${userAge}`);

let tag = `<div><h3>${userName}</h3></div><div><h4>${userAge}</h4></div>`;

document.write(tag);

array.forEach(i => {
  document.write(`${i} <br>`);
});

let number = 1234567890;

console.log(number.toLocaleString());

let px = '24px';

console.log(parseInt(px));

let random = (min, max) => {
  return Math.round(min + Math.random() * (max - min));

};

console.log(random(0, 10));
console.log(Math.random());