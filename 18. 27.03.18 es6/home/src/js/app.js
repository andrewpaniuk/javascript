// import '../sass/main.sass';
/*jshint esversion:6*/
const sass = require('../sass/main.sass');



function createListItem(title) {
  const titleLink = createElement('a', {
    href: '#'
  }, title);

  const span = createElement('span', {}, '0');

  const div = createElement('div', {}, titleLink, span);

  const addButton = createElement('button', {}, 'Delete');

  const listItem = createElement('li', {}, div, addButton);

  bindEvents(listItem);

  return listItem;
}


function addItemToListForm(e) {
  e.preventDefault();


  const list2 = document.querySelector('#list');
  const listItems2 = list.querySelectorAll('li');
  
  if (listFormInput.value === '') {
    alert(`Введите тему!`);
    return false;
  } else if (validation()) {
    alert(`Введите темуs!`);
    return false;
  }

  const listItem = createListItem(listFormInput.value);

  
  list.appendChild(listItem);
  upperCase();

}

function validation() {
  const list2 = document.querySelector('#list');
  const listItems2 = list.querySelectorAll('li');
  listItems2.forEach(li => {
    if (li.querySelector('a').innerText.toLowerCase() == listFormInput.value.toLowerCase()) {
      alert('Такая тема уже есть!');
      console.log(li.querySelector('a').innerText.toLowerCase() + ' ' + listFormInput.value.toLowerCase());
      return true;
    }
  });
}


const listForm = document.querySelector('#list-form');
const listFormInput = listForm.querySelector('.items__input');
const list = document.querySelector('#list');
const listItemSpan = list.querySelectorAll('span');
const deleveListItem = list.querySelectorAll('button');
const listItems = list.querySelectorAll('li');
const listItemsLink = list.querySelector('a');

function deleteItem(e) {
  const item = e.target.parentNode;
  list.removeChild(item);

}


function checkNumber() {
  listItemSpan.forEach(span => {
    if (span.innerText == '') {
      span.innerText = '0';
    }
  });
}

function upperCase() {
  const listTitle = list.querySelectorAll('a');
  listTitle.forEach(title => {
    title.innerText = title.innerText.charAt(0).toUpperCase() + title.innerText.slice(1);
  });
}

upperCase();
checkNumber();

listForm.addEventListener('submit', addItemToListForm);

listItems.forEach(item => bindEvents(item));

function bindEvents(item) {
  const btn = item.querySelector('button');
  btn.addEventListener('click', deleteItem);
}