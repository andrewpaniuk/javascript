const Extract = require('extract-text-webpack-plugin');

const extractSass = new Extract({
  filename: 'css/main.css'
});

module.exports = function () {
  return {
    module: {
      rules: [{
          test: /\.sass$/,
          use: extractSass.extract({
            use: [{
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  minimize: true
                }
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true,
                  minimize: true
                }
              }
            ],
            fallback: 'style-loader',
          }),
        },
        {
          test: /\.css$/,
          use: extractSass.extract({
            fallback: 'style-loader',
            use: [{
              loader: 'css-loader'
            }],
          }),
        }
      ]
    },
    plugins: [
      extractSass
    ]
  };
};