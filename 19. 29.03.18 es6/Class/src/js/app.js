/*jshint esversion:6*/
import '../sass/main.sass';

// let arr = [1, 2, 3, 4, 5];

// for (let item of arr) {
//   console.log(item);
// }

class Dog {
  constructor(name, age, bread) {
    console.log(`Dog constructor`);
    this._name = name;
    this._age = age;
    this._bread = bread;
    this._energy = 100;
    Dog.count += 1;
    // this.getInfo();
    // Dog.counter();
    this.init();
  }

  static counter() {
    this._counter += 1;
    console.log(`counter ${this._counter}`);
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    if (newName !== this._name && typeof newName !== 'number') {
      this._name = newName;
    }
    console.log(`Nelzaaaaaaaa cyfru stavit!`);
  }

  getInfo() {
    console.log(`${this._name} ${this._age} ${this._bread} ${this._energy}`);
  }

  play() {
    this._energy -= 20;
    if (this._energy <= 0) {
      console.log('Dog is dead!');
    }
    this.getInfo();
  }

  eat() {
    this._energy += 20;
    if (this._energy >= 100) {
      console.log('Dog is dead!');
    }
    this.getInfo();
  }

  sleep() {
    this._energy += 20;
    this.getInfo();
  }

  init() {
    const play = document.querySelector('#play');
    const eat = document.querySelector('#eat');
    const sleep = document.querySelector('#sleep');

    play.addEventListener('click', this.play.bind(this));
  }
}

class HunterDog extends Dog {
  constructor(name, age, bread, power) {
    super(name, age, bread);
    this._power = power;
    console.log(`HunterDog constructor`);
  }

  getInfo() {
    console.log(`${this._name} ${this._age} ${this._bread} ${this._energy} ${this._power}`);
  }

  hunt() {
    this.getInfo();
    let that = this;
    setInterval(() => {
      that._energy -= 1;
      that.getInfo();
    }, 1000);
  }

  get power() {
    return this._power;
  }
  set power(newPower) {
    this._power = newPower;
  }
}
Dog.count = 0;
// console.log(tuzik.name);
// console.log(tuzik.name = 0);

let tuzik = new Dog('Tuzik', 2, 'some');
let soska = new HunterDog('Soska', 65, 'super puper poroda', 3);

console.log(Dog.count);
// console.log(soska._power);

// soska.hunt();












const ADD_PERSON = (name, surname, age) => {
  return {
    name,
    surname,
    age,
    getHobby() {
      return 'Some hobby';
    }
  };
};

let person1 = ADD_PERSON('Johhny', 'Surname', 25);
let person2 = ADD_PERSON('a', 'sone', 1);
console.log(`${person1.name == person2.name}`);






























// tuzik.play();
// tuzik.play();
// tuzik.play();
// tuzik.eat();
// tuzik.play();
// tuzik.play();
// tuzik.play();
// tuzik.play();

// tuzik._name = 'bob';
// console.log(tuzik._name);
// console.log(tuzik);




// let dog1 = new Dog('dog1', 2, 'some');
// let dog2 = new Dog('dog2', 4, 'some');

// console.log(typeof Dog);
// console.log(dog1.name);
// console.log(Dog());


// Object.keys(dog1).forEach(key => {
//   console.log(key + ': ' + dog1[key]);
// });

// for (let key in dog1) {
//   console.log(key + ' ' + dog1[key]);
// }

// console.log(Object.keys(dog1));

// dog1.getInfo();
// dog2.getInfo();