import '../sass/main.sass';

// let arr = [3, 6, 9, 200, 214, 154];

// let sortArr = (a, b) => a + b;

// console.log(arr.sort(sortArr));


import {
  randomInt
} from './modules/helpers';


import Dog from './modules/dogs/dog';
import HuntDog from './modules/dogs/huntdog';
import SmallDog from './modules/dogs/smalldog';


import Cat from './modules/cats/cat';

// import { name, age } from './person';
// import * as Person from './person';
// import {name as a} as Person from './person';

// const billy = new Dog('Billy', 2, 'Normal', 95);
// billy.getInfo();
// billy.play(101);
// billy.eat(20);
// billy.name = 'NewName';
// console.log(billy.name);


const hunterJohn = new HuntDog('John', 1, 'Hunter',60, 49);
hunterJohn.getInfo();
// hunterJohn.hunt(3);

// const soska = new SmallDog('Soska', 2, 'AngryDog', 100);
// console.log(soska.isAngry);
// soska.bark();
// console.log(randomInt(0,1));




// let tolik = new Cat('Tolik', 1);
// tolik.getInfo();
// console.dir(tolik);

// let set = new Set();

// set.add(3);
// set.add(2);
// set.add(1);

// set.add(4).add(5).add(6).add(6).add(5);
// console.log(set);
// console.log(set.size);
// console.log(`Has: ${set.has(6)}`);
// console.log(`Delete: ${set.delete(5)}`);
// console.log(`Has 5: ${set.has(5)}`);


// let arr = new Set([1, 2, 3, 4, 5, 6, 6, 7, 7, 7 , 8, 8, 8]);

// console.log(arr);

// let map = new Map();

// console.dir(map);

// map.set('name', 'Billy');
// map.set('surname', 'Bone');
// map.set('age', 23);
// console.dir(map);
// console.log(map.get('name'));
// console.log(`Has: ${map.has('name')}`);
// map.delete('age');
// console.dir(map, map.size);

let arr = new Map([
  ['name', 'Billy'],
  ['surname', 'Bone'],
  ['age', 23]
]);

console.log(arr);

for (let item of arr.values()) {
  console.log(`Value: ${item}`);
}
for (let key of arr.keys()) {
  console.log(`Key: ${key}`);
}

for (let entry of arr.entries()) {
  console.log(`Key: ${entry[0]} || Value: ${entry[1]}`);
}

// console.log(randomInt(2,3));
// console.log(name, age);