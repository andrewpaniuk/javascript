let Cat = class {
  constructor(name, age) {
    this._name = name;
    this._age = age;
  }

  getInfo() {
    console.log(`Name: ${this._name} || Age: ${this._age}`);
  }
};
export default Cat;