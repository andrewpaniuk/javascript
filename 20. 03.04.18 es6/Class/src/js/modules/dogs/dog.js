class Dog {
  constructor(name, age, breed, energy = 100) {
    console.log(`Dog costructor`);
    Dog.count += 1;
    Dog.showCounter();
    this._name = name;
    this._age = age;
    this._breed = breed;
    this._energy = energy;
  }

  static showCounter() {
    console.warn(`Dog constructor has been started ${Dog.count} times`);
  }

  getInfo() {
    console.log(`Name: ${this._name} || Age: ${this._age} || Breed: ${this._breed} || Energy: ${this._energy}%`);
  }

  play(value) {
    this._energy -= value;
    if (this._energy < 0) {
      console.error(`${this._name} is tired! Its energy is very low ${this._energy}%.`);
      this.sleep();
    }
  }

  eat(value) {
    this._energy += value;
    if (this._energy > 100) {
      console.error(`${this._name} has eaten too much! It need so sleep!`);
      this.sleep();
    }
  }

  sleep() {
    this._energy = 100;
    console.warn(`${this._name} is sleeping! Don't bother it!`);
    // setTimeout(() => {
    //   this._energy = 100;
    //   console.log(`${this._name} is waked up! Now is has ${this._energy}% energy.`);
    // }, 2000);
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    if (typeof newName !== 'number' && this._name !== newName) {
      this._name = newName;
    } else {
      console.error(`This name is number or same name!`);
    }
  }
}

Dog.count = 0;

export default Dog;