class HuntDog extends Dog {
  constructor(name, age, breed, energy, power = 100) {
    console.log('HuntDog constructor');
    super(name, age, breed, energy);
    this._power = power;
  }

  getInfo() {
    console.log(`Name: ${this._name} || Age: ${this._age} || Breed: ${this._breed} || Energy: ${this._energy}% || Power: ${this._power}%`);
  }

  hunt(value) {
    let counter = 0;
    let hrHrs = 'hour';
    let timer = setInterval(() => {
      counter += value;
      this._power -= value;
      console.log(`${this._name} is hunting! It hunts ${counter} ${hrHrs}. Its power is ${this._power}%`);

      if (counter >= 9) {
        hrHrs = 'hours';
      }
      if (counter == 12) {
        console.error(`${this._name} has been hunting too much, it's tired! Its power is ${this._power}%.`);
        clearInterval(timer);
        this.sleep();
      }
    }, value * 1000);
  }

  sleep() {
    super.sleep();
    this._power = 100;
  }
}

import Dog from './dog';
export default HuntDog;