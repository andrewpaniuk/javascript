class SmallDog extends Dog {
  constructor(name, age, breed, energy, isAngry = true) {
    super(name, age, breed, energy);
    this._isAngry = isAngry;
  }

  get isAngry() {
    if (this._isAngry == true) {
      return `${this._name} is angry dog!`;
    } else {
      return `${this._name} isn't angry dog!`;
    }
  }

  bark() {
    if (this._isAngry == true) {
      console.warn(`${this._name} is barking and can't stop it!`);
    } else {
      console.log(`${this._name} is a good dog!`);
    }
  }
}

import Dog from './dog';
export default SmallDog;