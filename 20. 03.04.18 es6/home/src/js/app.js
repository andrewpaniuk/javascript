/*jshint esversion:6*/
import '../sass/main.sass';

import {
  randomInt
} from './helpers.js';

class Dog {
  constructor(name, age, breed, energy = 100) {
    console.log(`Dog costructor`);
    Dog.count += 1;
    Dog.showCounter();
    this._name = name;
    this._age = age;
    this._breed = breed;
    this._energy = energy;
  }

  static showCounter() {
    console.warn(`Dog constructor has been started ${Dog.count} times`);
  }

  getInfo() {
    console.log(`Name: ${this._name} || Age: ${this._age} || Breed: ${this._breed} || Energy: ${this._energy}%`);
  }

  play(value) {
    this._energy -= value;
    if (this._energy < 0) {
      console.error(`${this._name} is tired! Its energy is very low ${this._energy}%.`);
      this.sleep();
    }
  }

  eat(value) {
    this._energy += value;
    if (this._energy > 100) {
      console.error(`${this._name} has eaten too much! It need so sleep!`);
      this.sleep();
    }
  }

  sleep() {
    this._energy = 100;
    console.warn(`${this._name} is sleeping! Don't bother it!`);
    // setTimeout(() => {
    //   this._energy = 100;
    //   console.log(`${this._name} is waked up! Now is has ${this._energy}% energy.`);
    // }, 2000);
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    if (typeof newName !== 'number' && this._name !== newName) {
      this._name = newName;
    } else {
      console.error(`This name is number or same name!`);
    }
  }
}

Dog.count = 0;


// const billy = new Dog('Billy', 2, 'Normal', 95);
// billy.getInfo();
// billy.play(101);
// billy.eat(20);
// billy.name = 'NewName';
// console.log(billy.name);

class HuntDog extends Dog {
  constructor(name, age, breed, energy, power = 100) {
    console.log('HuntDog constructor');
    super(name, age, breed, energy);
    this._power = power;
  }

  getInfo() {
    console.log(`Name: ${this._name} || Age: ${this._age} || Breed: ${this._breed} || Energy: ${this._energy}% || Power: ${this._power}%`);
  }

  hunt(value) {
    let counter = 0;
    let hrHrs = 'hour';
    let timer = setInterval(() => {
      counter += value;
      this._power -= value;
      console.log(`${this._name} is hunting! It hunts ${counter} ${hrHrs}. Its power is ${this._power}%`);

      if (counter >= 9) {
        hrHrs = 'hours';
      }
      if (counter == 12) {
        console.error(`${this._name} has been hunting too much, it's tired! Its power is ${this._power}%.`);
        clearInterval(timer);
        this.sleep();
      }
    }, value * 1000);
  }

  sleep() {
    super.sleep();
    this._power = 100;
  }
}

class SmallDog extends Dog {
  constructor(name, age, breed, energy, isAngry = true) {
    super(name, age, breed, energy);
    this._isAngry = isAngry;
  }

  get isAngry() {
    if (this._isAngry == true) {
      return `${this._name} is angry dog!`;
    } else {
      return `${this._name} isn't angry dog!`;
    }
  }

  bark() {
    if (this._isAngry == true) {
      console.warn(`${this._name} is barking and can't stop it!`);
    } else {
      console.log(`${this._name} is a good dog!`);
    }
  }
}

// const hunterJohn = new HuntDog('John', 1, 'Hunter',60, 49);


// hunterJohn.getInfo();
// hunterJohn.hunt(3);

const soska = new SmallDog('Soska', 2, 'AngryDog', 100);
console.log(soska.isAngry);
soska.bark();
// console.log(randomInt(0,1));