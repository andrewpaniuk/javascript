import '../sass/main.sass';

import { createElement } from './modules/helpers';

function request(url) {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url, false);
  xhr.send();
  if (xhr.readyState == 4 && xhr.status == 200) {
    return JSON.parse(xhr.responseText);
  }
}

const usersURL = 'https://jsonplaceholder.typicode.com/users';
const postsURL = 'https://jsonplaceholder.typicode.com/posts';

const usersData = request(usersURL);
const postsData = request(postsURL);

const listNames = document.querySelector('#names');
const bio = document.querySelector('#bio');
const right = document.querySelector('#right');


function getUserName(i) {
  return usersData[i].name;
}

function getId(i) {
  return usersData[i].id;
}

function showNames() {
  for (let i = 0; i < usersData.length; i++) {
    listNames.innerHTML += `<span class="name" data-id="${getId(i)}">ID: ${getId(i)} NAME: ${getUserName(i)}</span>`;
  }
}

function showInfo(id) {
  bio.innerHTML = `<h3>Information about ${usersData[id].name}</h3>`;
  bio.innerHTML += `<p>Name: ${usersData[id].name}</p><p>UserName: ${usersData[id].username}</p><p>Email: ${usersData[id].email}</p><p>Phone: ${usersData[id].phone}</p><p>Website: ${usersData[id].website}</p>`;
}

function showPost(e) {
  let user = e.target;
  let userId = user.getAttribute('data-id');
  right.innerHTML = '<h1>Posts</h1>';
  for (let i = 0; i < postsData.length; i++) {
    if (postsData[i].userId == userId) {
     right.innerHTML += `<div class="post"><p class="title">${postsData[i].title}</p><p>${postsData[i].body}</p><?div>`;
    }
  }
  showInfo(userId - 1);
}


showNames();

window.addEventListener('load', function() {
  let names = document.querySelectorAll('.name');
  names.forEach(i => {
    i.addEventListener('click', showPost);
  });
});