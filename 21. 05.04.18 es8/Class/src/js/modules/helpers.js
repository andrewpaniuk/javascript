function createElement(tag, props, ...children) {
  let item = document.createElement(tag);

  Object.keys(props).forEach(key => item[key] = props[key]);

  if (children.length > 0) {
    children.forEach(child => {
      if (typeof child === 'string') {
        child = document.createTextNode(child);
      }
      item.appendChild(child);
    });
  }

  return item;

}

export { createElement };