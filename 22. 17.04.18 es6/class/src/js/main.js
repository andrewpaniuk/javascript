console.log('Webpack 4.5.0 <3');
//  ПРОМІСИ

let clientsDocs = {
  name: "Luc",
  surname: "Skywoker",
  passportId: "РC 002845",
  //prison : true,
  prison: false,
  //religion : "musulmanin"
  religion: "other"
};

function check(clientsDocs) {

  if (clientsDocs.prison === true) {
    return `Готель. Відмова. Заборонено приймати в'язнів`;
  } else if (clientsDocs.religion === 'musulmanin') {
    return `Авіа. Заборонено для мусульманів.`;
  } else {
    return true;
  }

}

function orderTrip(clientsDocs) {
  //console.log(clientsDocs);
  console.log(`Order in proccess ...`);

  let promise = new Promise(function (resolve, reject) {
    setTimeout(function () {

      let res = check(clientsDocs);

      if (res !== true) {
        reject(res);
      } else {
        resolve(clientsDocs);
      }

    }, 1000);
  });

  return promise;

}

function tourInfo(clientsDocs) {
  console.log(`Tour accepted`);
  console.log(clientsDocs);

  return new Promise(function (resolve, reject) {
    setTimeout(function () {

      let result = Math.random() > 0.2;

      if (result) {
        resolve(clientsDocs);
      } else {
        reject('Скасовано. Нема доступного туру.');
      }

    }, 1000);
  });

}

function bookingHotel(clientsDocs) {
  console.log(`Booking proccess ...`);

  return new Promise(function (resolve, reject) {

    let result = Math.random() > 0.15;

    if (result) {
      resolve({
        hotel: "Superresott",
        type: "AI"
      });
    } else {
      reject('Скасовано. Нема вільних місць у готелі');
    }



  });
}

function buyTickets(clientsDocs) {
  console.log(`Ticket proccess ...`);
  console.log(`Hotel `, clientsDocs);

  return new Promise(function (resolve, reject) {


    let result = Math.random() > 0.1;

    if (result) {
      resolve({
        transport: "FlyDubai",
        siteclass: "Econome"
      });
    } else {
      reject('Скасовано. Нема квитків на літак');
    }

  });
}

function goToTrip(clientsDocs) {
  console.log('Transport ', clientsDocs);
  console.log(`Go to trip`);
}

orderTrip(clientsDocs)
  .then(tourInfo)
  .then(bookingHotel)
  .then(buyTickets)
  .then(goToTrip)
  .catch(reason => console.log(reason));