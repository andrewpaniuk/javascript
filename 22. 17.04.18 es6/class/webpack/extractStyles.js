const extract = require('extract-text-webpack-plugin');

module.exports = function () {
  return {
    module: {
      rules: [{
        test: /\.(sass|scss)$/,
          use: extract.extract({
            use: [{
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  minimize: true
                }
              },
              {
                loader: 'postcss-loader',
                options: {
                  sourceMap: true,
                  minimize: true
                }
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true,
                  minimize: true
                }
              }
            ],
            fallback: 'style-loader',
          }),
        },
        {
          test: /\.css$/,
          use: extract.extract({
            fallback: 'style-loader',
            use: [{
                loader: 'css-loader'
              },
              {
                loader: 'group-css-media-queries-loader'
              }
            ],
          }),
        }
      ]
    },
    plugins: [
      new extract({
        filename: './css/main.css'
      })
    ]
  };
};