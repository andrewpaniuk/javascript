console.log('Webpack 4.5.0 <3');

let userDoc = {
  name: 'Johnny',
  surname: 'Cap',
  passportID: 'JC 352139',
  prison: false,
  work: true,
  payment: 3000,
  credit: 14000
};

let check = userDoc => {
  if (userDoc.prison === true) {
    return 'Sorry! But we have to cancel! Because you have been in prison';
  } else if (userDoc.work === false) {
    return 'Sorry! But we have to cancel! Because you haven\'t had a work enough';
  } else if (userDoc.credit > userDoc.payment * 10) {
    return 'Sorry! But we have to cancel! Because your payment is too small';
  } else {
    return true;
  }
};

let checkDoc = userDoc => {
  console.log(`checking documents...`);
  let promise = new Promise((resolve, reject) => {
    setTimeout(() => {

      let result = check(userDoc);

      if (result !== true) {
        reject(result);
      } else {
        resolve(userDoc);
      }
    }, 2000);
  });
  return promise;
};

let applyDoc = userDoc => {
  console.log(`Your documents are ready! And you can get your credit ${userDoc.credit} USD`);
  console.log(userDoc);
};

checkDoc(userDoc)
  .then(applyDoc)
  .catch(error => console.error(error));