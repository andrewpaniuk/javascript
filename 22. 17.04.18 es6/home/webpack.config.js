const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const babel = require('./webpack/babel');
const pug = require('./webpack/pug');
const svg = require('./webpack/svg');
const css = require('./webpack/css');
const sass = require('./webpack/sass');
const images = require('./webpack/images');
const fonts = require('./webpack/fonts');
const dev_server = require('./webpack/devServer');
const extract_styles = require('./webpack/extractStyles');
const html_webpack_plugin = require('html-webpack-plugin');
const clean_webpack_plugin = require('clean-webpack-plugin');
const uglify_js_plugin = require('uglifyjs-webpack-plugin');
const imagemin_plugin = require('imagemin-webpack-plugin').default;
const copy_webpack_plugin = require('copy-webpack-plugin');

let conf = merge([{

    entry: {
      app: [
        './src/js/main.js',
        './src/sass/main.sass'
      ]
    },

    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/bundle.js'
    },
    plugins: [
      new html_webpack_plugin({
        template: './src/index.pug'
      }),

      new copy_webpack_plugin(
        [{
          from: './src/img',
          to: 'img'
        }], {
          ignore: [{
            glob: 'svg/*.svg'
          }]
        }
      ),

      new imagemin_plugin({
        test: /\.(png|gif|jpeg|jpg|svg)$/i
      }),

      // new webpack.ProvidePlugin({
      //   $: 'jquery',
      //   jQuery: 'jquery'
      // })
    ],
  },
  babel(),
  pug(),
  svg(),
  images(),
  fonts()
]);

module.exports = (env, options) => {
  let production = options.mode === 'production';

  conf.devtool = production ? 'source-map' : 'cheap-eval-source-map';
  if (production) {
    return merge([
      conf,
      {
        plugins: [
          new clean_webpack_plugin(['dist']),

          new uglify_js_plugin({
            sourceMap: true
          }),

          new webpack.LoaderOptionsPlugin({
            minimize: true
          })
        ]
      },
      extract_styles()
    ]);
  } else {
    return merge([
      conf,
      dev_server(),
      sass(),
      css()
    ]);
  }
};