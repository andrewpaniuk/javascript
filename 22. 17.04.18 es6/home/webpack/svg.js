module.exports = function() {
  return {
    module: {
      rules: [
        {
          test: /\.svg$/,
          use: [
            {
              loader: 'svg-url-loader'
            }
          ]
        }
      ]
    }
  };
};