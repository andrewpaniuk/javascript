console.log('Webpack 4.5.0 <3');

// let path = 'http://omdbapi.com/?s=';
// let title = 'spider+man';
// let key = '&apikey=10971729';

// let url = path + title + key;

let content = document.querySelector('#content');
let input = document.querySelector('#input');
let submit = document.querySelector('#submit');
let more = document.querySelector('#more');

more.addEventListener('click', () => {

});

submit.addEventListener('click', () => {
  let path = 'http://omdbapi.com/?s=';
  let title = input.value.replace(' ', '+').toLowerCase();
  console.log(title);
  let key = '&apikey=10971729';

  let url = path + title + key;
  content.innerHTML = '';
  getData(url)
    .then(movies => {
      movies.forEach(value => {
        addPost(value);
        console.log(value);
      });
    })
    .catch(error => console.log(error));
});

function getData(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = () => {
      if (xhr.status == 200 && xhr.readyState == 4) {
        let data = JSON.parse(xhr.responseText);
        console.log(data.Search);
        resolve(data.Search);
      } else {
        reject(xhr.statusText);
      }
    };
    xhr.send();
  });
}

function addPost(movie) {
  let a = document.createElement('a');
  a.href = movie.Poster;
  a.target = '_blank';

  let span = document.createElement('span');
  span.innerText = movie.Title;
  let img = document.createElement('img');
  img.src = movie.Poster;
  a.appendChild(img);
  a.appendChild(span);
  content.appendChild(a);
}