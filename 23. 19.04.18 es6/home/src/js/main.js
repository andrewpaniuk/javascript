console.log('Webpack 4.5.0 <3');

let title = document.querySelector('#input');
let submit = document.querySelector('#submit');
let more = document.querySelector('#more');
let content = document.querySelector('#content');
let select = document.querySelector('#select');
let infoText = document.querySelector('#page_num');
let type = select.options[select.selectedIndex].value;

let url;
let pageNum;
let maxPageNum;

submit.addEventListener('click', () => {
  let path = 'http://www.omdbapi.com/?s=';
  let titleValue = title.value.toLowerCase().replace(' ', '+');
  url = path + titleValue + '&apikey=10971729';
  type = select.options[select.selectedIndex].value;
  getMovies(url)
    .then(data => {
      if (data.Response.toLowerCase() == 'false') {
        maxPageNum = null;
        infoText.innerHTML = '';
      } else {
        more.style.display = 'block';
        content.innerHTML = '';
        let movies = data.Search;
        maxPageNum = Math.ceil(data.totalResults / movies.length);
        pageNum = 2;
        movies.forEach(element => {
          addPosters(element, 1);
        });
      }
    })
    .catch(error => console.log(`SOMETHING WRONG: ${error}`));
});

more.addEventListener('click', () => {
  if (url && pageNum) {
    if (pageNum > maxPageNum) {
      return;
    }
    if (pageNum == maxPageNum) {
      more.style.display = 'none';
    }
    let nextPage = url + `&page=${pageNum}`;
    getMovies(nextPage)
      .then(data => {
        let movies = data.Search;
        movies.forEach(element => {
          addPosters(element, pageNum);
        });
        pageNum += 1;
      })
      .catch(error => console.log(`SOMETHING WRONG: ${error}`));
  }
});

function getMovies(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = () => {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          let data = JSON.parse(xhr.responseText);
          resolve(data);
        } else {
          let error = new Error(this.statusText);
          error.code = this.status;
          reject(error);
        }
      }
    };

    xhr.onerror = function () {
      reject(new Error('Network Error'));
    };

    xhr.send();
  });
}

function addPosters(movies, page) {
  infoText.innerHTML = `<h3>Page: ${page}</h3>`;
  if (type == 'all') {

  } else if (movies.Type !== type) {
    return;
  }
  let a = document.createElement('a');
  a.href = movies.Poster;
  a.target = '_blank';

  let span = document.createElement('span');
  span.innerText = movies.Title;
  let img = document.createElement('img');
  a.appendChild(img);
  a.appendChild(span);
  content.appendChild(a);
  if (movies.Poster == 'N/A') {
    // img.src = 'https://upload.wikimedia.org/wikipedia/commons/6/64/Poster_not_available.jpg';
    // img.src = 'http://i.imgur.com/SaYmoW6.gif';
    // img.src = 'http://teamjimmyjoe.com/wp-content/uploads/2018/01/baby-licking-window-glass.gif';
    // img.src = 'https://media1.tenor.com/images/b5a9f1e9bf32dcb5d338c3fadead5e3e/tenor.gif?itemid=5785816';
    img.src = 'https://78.media.tumblr.com/6f1c3221fa163e445f5dca3925539bc3/tumblr_osive3ucdp1udh5n8o1_250.gif';
  } else {
    img.src = movies.Poster;
  }
}