module.exports = function () {
  return {
    module: {
      rules: [{
        test: /\.css$/,
        use: [{
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'group-css-media-queries-loader'
          }
        ],
      }]
    }
  };
};