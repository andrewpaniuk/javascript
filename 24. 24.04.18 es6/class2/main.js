// console.log(window);

// function myFunc() {
//   first();
//   second();
// }

// function first() {
//   console.log(`first`);
// }

// let second = function () {
//   console.log(`second`);
// };

// myFunc();


// var a = 1;

// function test() {
//   var a = 25;
//   console.log(a + ' tesst');
// }

// test();
// console.log(a);



// var test = function() {
//   console.log('test2');
// };

// test();

// =====================================================

// (function () {

//   function $() {

//   }

//   function hello() {
//     console.log('hello');
//   }

//   function world() {
//     console.log('world');
//   }

//   $.hello = hello;
//   $.world = world;

//   window.$ = $;

// }());

// $.hello();

// ==================================

// var app = {};

// if (typeof app === 'undefined') {
//   var app = {};
//   console.log('created');
// } else {
//   console.log('exist');
// }
// if (typeof app === 'undefined') {
//   var app = {};
//   console.log('created');
// } else {
//   console.log('exist');
// }
// =========================================

var app = app || {};

app.define = function(nameSpace) {
  var parts = nameSpace.split('.'),
  parent = app,
  i;
  if (parts[0] == 'app') {
    parts = parts.slice(1);
  }
  
  console.log(parts);

  for (i = 0; i < parts.length; i++) {
    if (typeof parent[parts[i]] === 'undefined') {
      parent[parts[i]] = {};
    }
    parent = parent[parts[i]];
  }
  return parent;
};

app.define('utils.calc');

app.utils.calc = (function() {
  // console.log('calc');
  var x, y;

  return {
    add: function() {
      return x + y;
    },
    
    minus: function() {
      return x - y;
    },

    setX: function(value) {
      x = value;
    },

    setY: function(value) {
      y = value;
    }
  };
}());

var calc = app.utils.calc;

calc.setX(2);
calc.setY(3);
console.log(calc.add());

app.define('utils.showScreen');

app.utils.showScreen = (function() {
  var w = window.screen.width;
  var h = window.screen.height;
  return {
    width: function() {
      return w;
    },
    height: function() {
      return h;
    }
  };
}());

var screen = app.utils.showScreen;

console.log(screen.width());
console.log(screen.height());