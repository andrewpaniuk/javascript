var app = app || {};

app.define = function (spaceName) {
  var parts = spaceName.split('.'),
    parent = app,
    i;
  if (parts[0] == 'app') {
    parts = parts.slice(1);
  }

  for (i = 0; i < parts.length; i++) {
    if (typeof parent[parts[i]] === 'undefined') {
      parent[parts[i]] = {};
    }
    parent = parent[parts[i]];
  }
  return parent;
};

app.define('animal.cat');

// =================================

// app.animal.cat = (function () {
//   var _name,
//     _age,
//     _breed;
//   return {
//     set name(val) {
//       _name = val;
//     },

//     set age(val) {
//       _age = val;
//     },

//     set breed(val) {
//       _breed = val;
//     },

//     get name() {
//       return _name;
//     },

//     get age() {
//       return _age;
//     },

//     get breed() {
//       return _breed;
//     },

//     show: function () {
//       console.log('Name: ' + _name + ' | ' + 'Age: ' + _age + ' | ' + 'Breed: ' + _breed);
//     }
//   };
// })();


// app.animal.dog = (function () {
//   var _name,
//     _age,
//     _breed;
//   return {
//     set name(val) {
//       _name = val;
//     },

//     set age(val) {
//       _age = val;
//     },

//     set breed(val) {
//       _breed = val;
//     },

//     get name() {
//       return _name;
//     },

//     get age() {
//       return _age;
//     },

//     get breed() {
//       return _breed;
//     },

//     show: function () {
//       console.log('Name: ' + _name + ' | ' + 'Age: ' + _age + ' | ' + 'Breed: ' + _breed);
//     }
//   };
// })();

// ================================

app.animal.dog = (function () {
  var _name,
    _age,
    _breed;

  function setName(val) {
    _name = val;
  }

  function setAge(val) {
    _age = val;
  }

  function setBreed(val) {
    _breed = val;
  }

  function getName() {
    return _name;
  }

  function getAge() {
    return _age;
  }

  function getBreed() {
    return _breed;
  }

  function show() {
    console.log('Name: ' + _name + ' | ' + 'Age: ' + _age + ' | ' + 'Breed: ' + _breed);
  }
  return {
    setName: setName,
    setAge: setAge,
    setBreed: setBreed,

    getName: getName,
    getAge: getAge,
    getBreed: getBreed,

    show: show
  };
})();
app.animal.cat = (function () {
  var _name,
    _age,
    _breed;

  function setName(val) {
    _name = val;
  }

  function setAge(val) {
    _age = val;
  }

  function setBreed(val) {
    _breed = val;
  }

  function getName() {
    return _name;
  }

  function getAge() {
    return _age;
  }

  function getBreed() {
    return _breed;
  }

  function show() {
    console.log('Name: ' + _name + ' | ' + 'Age: ' + _age + ' | ' + 'Breed: ' + _breed);
  }
  return {
    setName: setName,
    setAge: setAge,
    setBreed: setBreed,

    getName: getName,
    getAge: getAge,
    getBreed: getBreed,

    show: show
  };
}());
// 