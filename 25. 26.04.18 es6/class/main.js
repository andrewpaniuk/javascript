// console.log(window);

// function myFunc() {
//   first();
//   second();
// }

// function first() {
//   console.log(`first`);
// }

// let second = function () {
//   console.log(`second`);
// };

// myFunc();


// var a = 1;

// function test() {
//   var a = 25;
//   console.log(a + ' tesst');
// }

// test();
// console.log(a);



// var test = function() {
//   console.log('test2');
// };

// test();

// =====================================================

// (function () {

//   function $() {

//   }

//   function hello() {
//     console.log('hello');
//   }

//   function world() {
//     console.log('world');
//   }

//   $.hello = hello;
//   $.world = world;

//   window.$ = $;

// }());

// $.hello();

// ==================================

// var app = {};

// if (typeof app === 'undefined') {
//   var app = {};
//   console.log('created');
// } else {
//   console.log('exist');
// }
// if (typeof app === 'undefined') {
//   var app = {};
//   console.log('created');
// } else {
//   console.log('exist');
// }
// =========================================



// var calc = app.utils.calc;

// calc.setX(2);
// calc.setY(3);
// console.log(calc.add());

// var screen = app.utils.showScreen;

// console.log(screen.width());
// console.log(screen.height());


// ======================================

// var bobik = app.animal.cat;
// bobik.name = 'Bobik';
// bobik.age = 2;
// bobik.breed = 'Breed cat';
// bobik.show();


// var doggy = app.animal.dog;
// doggy.name = 'Doggy';
// doggy.age = 3;
// doggy.breed = 'Breed dog';
// doggy.show();

var doggy = app.animal.dog;
doggy.setName('Doggy');
doggy.setAge(3);
doggy.setBreed('Breed dog');
doggy.show();

var bobik = app.animal.cat;
bobik.setName('Bobik');
bobik.setAge(1);
bobik.setBreed('Breed cat');
bobik.show();


// ========================= 🔥🔥🔥🔥🔥
// var obj1 = {
//   name: 'Test'
// };
// var obj2 = {
//   name: 'Test'
// };

// console.log(obj1 === obj2);
// console.log(obj1 == obj2);

// var obj3 = obj1;

// console.info(obj1 === obj3);
// console.info(obj1 == obj3);


//===================🔥

function Universe() {
    // if (Universe instanceof Object) {
    //   return Universe.instance;
    // }
    if (typeof Universe.instance === 'object') {
        return Universe.instance;
    }

    this._name = 'space';
    this._size = 'big';

    Universe.instance = this;
}

var space1 = new Universe();
var space2 = new Universe();

console.log(space1 === space2);
console.log(Universe.instance);


function CarMaker() {

}

CarMaker.prototype.drive = function () {
    console.log('вж вж вж вж..........' + this.doors);
};

CarMaker.create = function (type) {
    var constr = type,
        newCar;

    if (typeof CarMaker[constr] !== 'function') {
        throw {
            name: 'Error',
            message: constr + ' Error!'
        };
    }

    if (typeof CarMaker[constr].prototype.drive !== 'function') {
        CarMaker[constr].prototype = new CarMaker();
    }

    newCar = new CarMaker[constr]();

    return newCar;
};

CarMaker.Compact = function () {
    this.doors = 4;
};
CarMaker.Sedan = function () {
    this.doors = 4;
};
CarMaker.Universal = function () {
    this.doors = 5;
};

var car = CarMaker.create('Compact');
var car1 = CarMaker.create('Universal');

car.drive();
car1.drive();