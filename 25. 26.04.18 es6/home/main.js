// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 best

var Universe;
(function () {
    var instance;

    Universe = function Universe() {
        if (instance) {
            return instance;
        }

        instance = this;

        this._name = 'space';
        this._size = 'big';
    };
}());


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
// function Universe() {
//     // if (Universe instanceof Object) {
//     //   return Universe.instance;
//     // }
//     if (typeof Universe.instance === 'object') {
//         return Universe.instance;
//     }

//     this._name = 'space';
//     this._size = 'big';

//     Universe.instance = this;

//     return this;
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
// function Universe() {
//     var instance = this;

//     this._name = 'space';
//     this._size = 'big';

//     Universe = function() {
//         return instance;
//     };
// }

// добавить свойство в прототип
Universe.prototype.nothing = true;
var uni = new Universe();
// добавить еще одно свойство в прототип
// уже после создания первого объекта
Universe.prototype.everything = true;
var uni2 = new Universe();

console.log(uni.nothing);
console.log(uni2.nothing);
console.log(uni.everything);
console.log(uni2.everything);

// это выражение дает ожидаемый результат:
console.log(uni.constructor.name);
// “Universe”
// а это нет:
console.log(uni.constructor === Universe); // false


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 best
// function Universe() {
//     var instance;

//     Universe = function Universe() {
//         return instance;
//     };

//     Universe.prototype = this;

//     instance = new Universe();

//     instance.constructor = Universe;

//     this._name = 'space';
//     this._size = 'big';

//     return instance;

// }

// var space1 = new Universe();
// var space2 = new Universe();

// console.log(space1 === space2);
// console.log(Universe.instance);

