function Animal() {

}

Animal.prototype.run = function () {
    console.log('Run...' + this._legs + ' legs.');
};
Animal.prototype.setName = function (val) {
    this._name = val;
};
Animal.prototype.setAge = function (val) {
    this._age = val;
};
Animal.prototype.info = function (name) {
    console.log('Name: ' + this._name + ' | ' + 'Age: ' + this._age);
};

Animal.add = function (type) {
    var constr = type,
        newAnimal;

    if (typeof Animal[constr] !== 'function') {
        throw {
            name: 'Error',
            message: constr + ' Error!'
        };
    }

    // if (typeof Animal[constr].prototype.run !== 'function') {
    Animal[constr].prototype = new Animal();
    // }

    newAnimal = new Animal[constr]();

    return newAnimal;
};

Animal.Cat = function () {
    this._legs = 4;
};
Animal.Dog = function () {
    this._legs = 4;
};

var doggy = Animal.add('Dog');
doggy.setName('Doggy');
doggy.setAge(2);
doggy.info();
doggy.run();