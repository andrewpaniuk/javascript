// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

// var Water = function () {};

// Water.create = function (water) {
//     var constr = water,
//         newWater;

//     if (typeof Water[constr] !== 'function') {
//         throw {
//             name: 'Error',
//             message: 'Error'
//         };
//     }

//     Water[constr].prototype = new Water();

//     newWater = new Water[constr]();

//     return newWater;
// };

// Water.prototype.show = function () {
//     console.log(this._name);
// };

// Water.Cola = function () {
//     this._name = 'Coca-cola';
//     this._color = 'Black';
// };
// Water.Pepsi = function () {
//     this._name = 'Pepsi';
//     this._color = 'Black';
// };

// var cola = Water.create('Cola');
// var pepsi = Water.create('Pepsi');
// // console.log(cola._name);
// cola.show();


// iteration 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

// var collection = (function(){
//     var current = -1,
//     data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
//     dataLength = data.length;

//     return {
//         moveNext() {
//             if (current == dataLength - 1) {
//                 return false;
//             } else {
//                 current++;
//                 return true;
//             }
//         },

//         getCurrent() {
//             return data[current];
//         },

//         reset() {
//             current = -1;
//         }
//     };
// }());

// while (collection.moveNext()) {
//     var temp = collection.getCurrent();
//     console.log(temp);
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

// var validator = {
//     types: {},
//     messages: [],
//     config: {},

//     validate(data) {
//         var checker,
//             msg,
//             type,
//             invalid,
//             i;

//         this.message = [];

//         for (i in data) {
//             if (data.hasOwnProperty(i)) {
//                 type = this.config[i];
//                 checker = this.types[type];

//                 if (!type) {
//                     continue;
//                 }

//                 if (!checker) {
//                     throw {
//                         name: 'Validator Error',
//                         message: 'Validator not found ' + type
//                     };
//                 }

//                 invalid = checker.validate(data[i]);

//                 if (!invalid) {
//                     msg = "Error  " + i + ", " + checker.message;
//                     this.messages.push(msg);
//                 }
//             }
//         }
//         return this.hasErrors();
//     },

//     hasErrors() {
//         return this.messages.length !== 0;
//     }
// };

// validator.types.required = {
//     validate: function (value) {
//        //  return value !== '';
//         return !/[^a­z0­9]/i.test(value);
//     },
//     message: 'Required'
// };


// validator.config = {
//     firstName: 'required',
//     lastName: 'required',
//     age: 'number',
//     email: 'email'
// };

// validator.types.number = {
//     validate: function (value) {
//         return !/\d+/.test(value);
//         // return !isNaN(value);
//     },
//     message: "Must be a number"
// };

// validator.types.email = {
//     validate: function (value) {
//         // var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//         // return value.match(mailFormat);
//         return !/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/i.test(value);
//     },
//     message: "Must be an email address"
// };

// // var user = {
// //     firstName: 'qwe',
// //     lastName: '',
// //     age: 'sss',
// //     email: 'and'
// // };

// var data1 = {
//     firstName: "Tom",
//     lastName: "Tomson",
//     age: 35,
//     email: "tomson@ry.ru"
// };

// if (validator.validate(data1)) {
//     console.dir(validator.messages);
// }

// var result = validator.validate(data1);
// console.log(result);



// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
var validator = {
    // все доступные проверки
    types: {},
    // сообщения об ошибках 
    // в текущем сеансе проверки
    messages: [],

    // текущие параметры проверки
    // имя: тип проверки
    config: {},
    // интерфейсный метод
    // аргумент `data` – это пары ключ => значение
    validate: function (data) {
        var i, msg, type, checker, result_ok;
        // удалить все сообщения
        this.messages = [];
        for (i in data) {
            if (data.hasOwnProperty(i)) {
                type = this.config[i];
                checker = this.types[type];
                if (!type) {
                    continue; // проверка не требуется
                }
                if (!checker) {
                    throw {
                        name: 'ValidationError',
                        message: 'No handler to validate type' + type
                    };
                }
                result_ok = checker.validate(data[i]);
                console.log(result_ok);
                if (!result_ok) {
                    msg = 'Invalid value for *' + i + '*, ' + checker.instructions;
                    this.messages.push(msg);
                }
            }
        }
        return this.hasErrors();
    },
    // вспомогательный метод
    hasErrors: function () {
        return this.messages.length !== 0;
    }
};



validator.config = {
    first_name: 'isNonEmpty',
    last_name: 'isNonEmpty',
    age: 'isNumber',
    username: 'isAlphaNum'
};

// проверяет наличие значения
validator.types.isNonEmpty = {
    validate: function (value) {
        return value !== '';
        // return !!/([^a­zAZ0­9])/i.test(value);
    },
    instructions: 'the value cannot be empty'
};
// проверяет, является ли значение числом
validator.types.isNumber = {
    validate: function (value) {
        return !isNaN(value);
    },
    instructions: 'the value can only be a valid number, e.g. 1, 3.14 or 2010'
};
// проверяет, содержит ли значение только буквы и цифры
validator.types.isAlphaNum = {
    validate: function (value) {
        return !!/^[a­z09]/i.test(value);
    },
    instructions: 'the value can only contain characters and numbers, no special symbols'
};

var data = {
    first_name: '',
    last_name: 'Man',
    age: '3',
    username: 'ZZZ'
};

var res = validator.validate(data);
// validator.validate(data);
// console.log(res);

if (validator.hasErrors()) {
    console.log(validator.messages.join('\n'));
}
