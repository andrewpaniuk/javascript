// 🔥🔥🔥🔥🔥

// var Greeter = function(strategy) {
//     this.strategy = strategy;
// };

// Greeter.prototype.greet = function() {
//     return this.strategy();
// };

// var politeGreetingStrategy = function() {
//     console.log('Hello / polite');
// };

// var friendlyGreetingStrategy = function() {
//     console.log('Hey / friendly');
// };

// var boredGreetingStrategy = function() {
//     console.log('Sup / bored');
// };

// var politeGreeter = new Greeter(politeGreetingStrategy);
// var friendlyGreeter = new Greeter(friendlyGreetingStrategy);
// var boredGreeter = new Greeter(boredGreetingStrategy);

// politeGreeter.greet();
// friendlyGreeter.greet();
// boredGreeter.greet();



// 🔥🔥🔥🔥🔥

var Greeter = function(strategy) {
    this.strategy = strategy;
};

var Strategy = function() {};

Strategy.prototype.execute = function() {
    throw new Error('Strategy#executed needs to be overridden.');
};

var GreetingStrategy = function() {};
GreetingStrategy.prototype = Object.create(Strategy.prototype);

GreetingStrategy.prototype.execute = function() {
    return this.sayHi() + this.sayBye();
};

GreetingStrategy.prototype.sayHi = function() {
    return 'Hello, ';
};

GreetingStrategy.prototype.sayBye = function() {
    return 'Goodbye.';
};

Greeter.prototype.greet = function() {
    return this.strategy.execute();
};

var greeter = new Greeter(new GreetingStrategy());

console.log(greeter.greet());

var politeGreetingStrategy = function() {};
politeGreetingStrategy.prototype = Object.create(GreetingStrategy.prototype);
politeGreetingStrategy.prototype.sayHi = function() {
    return 'Welcome sir, ';
};

var friendlyGreetingStrategy = function() {};
friendlyGreetingStrategy.prototype = Object.create(GreetingStrategy.prototype);
friendlyGreetingStrategy.prototype.sayHi = function() {
    return 'Hey, ';
};

var boredGreetingStrategy = function() {};
boredGreetingStrategy.prototype = Object.create(GreetingStrategy.prototype);
boredGreetingStrategy.prototype.sayHi = function() {
    return 'Sup, ';
};

var politeGreeter = new Greeter(new politeGreetingStrategy());
var friendlyGreeter = new Greeter(new friendlyGreetingStrategy());
var boredGreeter = new Greeter(new boredGreetingStrategy());

console.log(politeGreeter.greet());
console.log(friendlyGreeter.greet());
console.log(boredGreeter.greet());


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

// var validator = {
//     types: {},
//     messages: [],
//     config: {},

//     validate: function(data) {
//         var i, msg, type, invalid, checker;

//         for (i in data) {
//             if (data.hasOwnProperty(i)) {
//                 type = this.config[i];
//                 if (!type) {
//                     continue;
//                 }
//                 checker = this.types[type];
//                 if (!checker) {
//                     throw {
//                         name: "ValidatorError",
//                         message: "Validator not found " + type
//                     };
//                 }

//                 invalid = checker.validate(data[i]);
//                 console.log(invalid);
//                 if (invalid) {
//                     msg = "Error " + i + ", " + checker.message;
//                     this.messages.push(msg);
//                 }
//             }
//         }

//         return this.hasErrors();
//     },

//     hasErrors: function() {
//         return this.messages.length !== 0;
//     }
// };

// validator.types.required = {
//     validate: function(value) {
//         return value === "";
//     },
//     message: "Required"
// };

// validator.types.number = {
//     validate: function(value) {
//         return isNaN(value);
//     },
//     message: "Must be a number"
// };

// validator.types.email = {
//     validate: function(value) {
//         return !/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/i.test(value);
//     },
//     message: "Must be an email address"
// };

// validator.config = {
//     firstName: "required",
//     lastName: "required",
//     age: "number",
//     email: "email"
// };

// obj1 = {
//     firstName: "Tom",
//     lastName: "sd",
//     age: '25',
//     email: "nnn@gmail.com"
// };

// var result = validator.validate(obj1);
// console.log(result);
// if (result) {
//     console.dir(validator.messages);
// }



// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

// const logger = function(strategy, lvl, msg, ...args) {
//     return strategy(lvl, msg, ...args);
// };
// const logToConsoleStrategy = function(lvl, msg) {
//     console[lvl](msg);
// };
// const logToDOMStrategy = function(lvl, msg, node) {
//     node.innerHTML = `<div class='${lvl}'>${msg}</div>`;
// };
// logger(
//     logToConsoleStrategy,
//     'log',
//     'Hello console'
// );
// logger(
//     logToDOMStrategy,
//     'warn',
//     'Hello DOM',
//     document.querySelector('#log')
// );

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

const logger = function(conf) {
    let strategy = conf.strategy,
        lvl = conf.level,
        msg = conf.message,
        args = conf.arguments || '';
    return strategy(lvl, msg, ...args);
};

const logToConsoleStrategy = function(lvl, msg) {
    console[lvl](msg);
};

const logToDOMStrategy = function(lvl, msg, node) {
    node.innerHTML += `<div class='${lvl}'>${msg}</div>`;
};

logger({
    strategy: logToConsoleStrategy,
    level: 'log',
    message: 'What\'s up console ⛑'
});
logger({
    strategy: logToDOMStrategy,
    level: 'log',
    message: 'What\'s up DOM 🏠',
    arguments: [document.querySelector('#log')]
});