let get = id => {
    return document.querySelector(id);
};
// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

// let callback = data => {
//     console.log(data);
//     get('#loader').style.display = 'none';
//     get('#output').innerHTML += `${data} <br>`;
// };
// let http = {
//     makeRequest(id, callback) {
//         setTimeout(function() {
//             callback(`response from server ${new Date()}`);
//         }, 2000);
//     }
// };
// let http = {
//     makeRequest(id, callback) {
//         setTimeout(function() {
//             callback(`response from server ${new Date()}`);
//         }, 2000);
//     }
// };

// let proxy = (function() {
//     let cache = {};
//     return {
//         makeRequest(id, callback) {
//             if (cache[id]) {
//                 callback(id);
//                 console.log(cache);
//             } else {
//                 http.makeRequest(id, function(data) {
//                     cache[id] = data;
//                     callback(data);
//                 });
//             }
//         }
//     };
// }());




// get('#btn--request').addEventListener('click', function() {
//     get('#loader').style.display = '';
//     let id = get('#input--search').value;
//     http.makeRequest(id, callback);
// });

// get('#btn--proxy').addEventListener('click', function() {
//     get('#loader').style.display = '';
//     let id = get('#input--search').value;
//     proxy.makeRequest(id, callback);
// });
// 🔥🔥🔥🔥🔥🔥🔥

let callback = data => {
    console.log(data);
    get('#loader').style.display = 'none';
    get('#output').innerHTML += `${data} <br>`;
};

let http = {
    makeRequest(id, data, callback) {
        setTimeout(function() {
            callback(`${data[id]}`);
        }, 2000);
    }
};


let proxy = (function() {
    let cache = {};
    return {
        makeRequest(id, data, callback) {
            if (cache[id]) {
                callback(cache[id]);
                console.log(cache);
            } else {
                http.makeRequest(id, data, function() {
                    let i;
                    console.log(data[id]);
                    for (i in data) {
                        if (id == i) {
                             callback(data[i]);
                             cache[i] = data[i];
                        }
                    }
                    console.log(cache);
                });
            }
        }
    };
}());

let dict1 = {
    cat: 'ukr Kit',
    dog: 'ukr Pes',
    snake: 'ukr Zmija'
};

let dict2 = {
    cat: 'rus Kot',
    dog: 'rus Co6aka',
    snake: 'rus Zmeja',
};

let random = [dict1, dict2];
let randomLength = random.length - 1;

function randomInt(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return +rand;
}

get('#btn--request').addEventListener('click', function() {
    get('#loader').style.display = '';
    let id = get('#input--search').value.toLowerCase();
    http.makeRequest(id, random[randomInt(0, randomLength)], callback);
});

get('#btn--proxy').addEventListener('click', function() {
    get('#loader').style.display = '';
    let id = get('#input--search').value.toLowerCase();
    proxy.makeRequest(id, random[randomInt(0, randomLength)], callback);
});

