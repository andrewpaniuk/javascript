// 🔥🔥🔥🔥🔥 Observer

// function Observable() {
//     let observers = [];

//     this.sendMessage = function(msg) {
//         for (let i = 0, max = observers.length; i < max; i++) {
//             observers[i].notify(msg);
//         }
//     };

//     this.addObserver = function(observer) {
//         observers.push(observer);
//     };

// }

// function Observer(behavior) {
//     this.notify = function(msg) {
//         behavior(msg);
//     };
// }

// let observable = new Observable();

// let obs1 = new Observer(function(msg) {
//     console.log(msg);
// });
// let obs2 = new Observer(function(msg) {
//     alert(msg);
// });

// observable.addObserver(obs1);
// observable.addObserver(obs2);

// setTimeout(function() {
//     observable.sendMessage('Time: ' + new Date());
// }, 2000);

// 🔥🔥🔥🔥🔥 Observer

function Observable() {
    let observers = [];

    this.sendMessage = function(msg) {
        for (let i = 0, max = observers.length; i < max; i++) {
            observers[i].notify(msg);
        }
    };

    this.addObserver = function(observer) {
        observers.push(observer);
    };

}

function Observer(behavior) {
    this.notify = function(msg) {
        behavior(msg);
    };
}

let observable = new Observable();

let basketObs = new Observer(function(id) {
    document.querySelector('.basket__products-list').innerHTML += `<li class="basket-product">Товар ${id}</li>`;
});

function removeModal() {
    document.querySelector('.buy-modal').classList.remove('hide');
    document.querySelector('.overlay').classList.remove('hide');
}
function addModal() {
    document.querySelector('.buy-modal').classList.add('hide');
    document.querySelector('.overlay').classList.add('hide');
}

let modalObs = new Observer(function(id) {
    let msg = `Товар ${id} добавлен в корзину!`;
    document.querySelector('.buy-modal__msg').innerText = msg;
    removeModal();
    setTimeout(function() {
        addModal();
    }, 2000);
});

let serverObs = new Observer(function(id) {
   console.log(`ID: ${id}`);
});

observable.addObserver(basketObs);
observable.addObserver(modalObs);
observable.addObserver(serverObs);

let products = document.querySelectorAll('.product');

products.forEach(product => {
    product.addEventListener('click', function(e) {
        let id = e.target.getAttribute('data-id');
        observable.sendMessage(id);
    });
});

document.querySelector('.buy-modal').addEventListener('click', function() {
    addModal();
});
document.querySelector('.overlay').addEventListener('click', function() {
    addModal();
});
// setTimeout(function() {
    // observable.sendMessage('Time: ' + new Date());
// }, 2000);