$(document).ready(function() {
    console.log(`I'm ready to fight!`);

    var $btnOrder = $('#btn-order');

    var logos = ['img/logo.png', 'img/logo2.png', 'https://www.freelogodesign.org/img/logo-ex-7.png'];
    var colors = ['yellow', 'green', 'blue', 'black', 'red'];

    var $logo = $('.logo img');
    // $logo.addClass('yellow');

    $btnOrder.on('click', function() {
        var srcLogo = $logo.attr('src');
        if (srcLogo == logos[0]) {
            $logo.attr('src', logos[1]);
            logos.push(logos[0]);
            logos.shift();
        }
        // console.log(srcLogo);

        // tak ploho 🔥🔥🔥
        // if ($logo.hasClass('yellow')) {
        //     console.log('sd');
        //     $logo.removeClass('yellow');
        //     $logo.addClass('green');
        // } else {
        //     $logo.removeClass('green');
        //     $logo.addClass('yellow');
        // }
        if ($logo.hasClass(colors[0])) {
            $logo.removeClass(colors[0]);
            $logo.addClass(colors[1]);
            colors.push(colors[0]);
            colors.shift();
        } else {
            $logo.addClass(colors[0]);
        }


    });

    // 🔥🔥🔥🔥🔥
    // $submit = $('#submit');

    // $submit.on('click', function(e) {
    //     $('input').each(function() {
    //         console.log(`Name = ${$(this).attr('name')} | Value = ${$(this).val()}`);
    //     });
    //     $('select').each(function() {
    //         console.log(`Name = ${$(this).attr('name')} | Value = ${$('option:selected', this).val()}`);
    //     });
    //     $('input[type="radio"]').each(function() {
    //         console.log(`Name = ${$('input[type="radio"]:radio').attr('name')} | Value = ${$('input[type="radio"]:radio').val()}`);
    //     });
    // });

    // var html = $('.section-title').html('<h2>new text</h2>');
    // var msg = 'werewr'.toUpperCase();
    // $('.section-title').text('<h2>new text</h2>'.toUpperCase());


    // var text = $('.title').text();
    // $('.section-title').text(text);
    // console.log('1');

    var firstName = $('input[name=first-name]');
    var lastName = $('input[name=last-name]');
    var email = $('input[name=email]');
    var pass1 = $('input[name=password1]');
    var pass2 = $('input[name=password2]');

    $submit = $('#submit');

    email.change(function() {
        if ($(this).val() !== '' && $(this).focusout()) {
            pass1.attr('type', 'password');
            pass2.attr('type', 'password');
        }
    });
    pass1.change(function() {
        pass1.val();
        pass2.val();
        if (pass1.val() !== pass2.val()) {
            pass2.css({
                border: '2px solid red'
            });
        } else {
            pass1.css({
                border: '2px solid green'
            });
            pass2.css({
                border: '2px solid green'
            });
        }
    });
    pass2.change(function() {
        pass1.val();
        pass2.val();
        if (pass1.val() !== pass2.val()) {
            pass2.css({
                border: '2px solid red'
            });
        } else {
            pass1.css({
                border: '2px solid green'
            });
            pass2.css({
                border: '2px solid green'
            });
        }
    });

    $submit.on('click', function() {

        if (email.val().indexOf('@') == -1) {
            alert('wrong email');
            return false;
        }
        if (pass1.val() !== pass2.val()) {
            alert('wrong password');
            return false;
        }
        if (firstName.val() !== '' && lastName.val() !== '' && email.val() !== '' && pass1.val() !== '' && pass2.val() !== '') {
            alert(`First name: ${firstName.val()} || Last name: ${lastName.val()} || Email: ${email.val()}`);
        } else {
            alert('something wrong');
            return false;
        }
    });

});