// (function() {
//   document.write("Test");
// })();
//
//
// Recurs
// function test(counter){
//   document.write("<p style=\"color: red;\">" + counter + "</p>");
//   counter--;
//   if(counter != 0) {
//     test(counter);
//   };
// };
//
// test(10);



// var func = function(){
//   document.write("Litt. func.");
// };
// func();


// function external(){
//   function interval(){
//     document.write("interval");
//   };
//   interval();
// };
// external();

//result === return of functions;
// var plus = function(a, b){
//   return a + b;
// };
// var minus = function(a, b){
//   return a - b;
// };
// function start(callBack, a, b){
//   var result = callBack(a, b);
//   document.write("<p style=\"color: red;\">" + result + "</p>");
// };
//
// start(minus, 10, 20);



var array = [1, 2, 3, "Hello", true, [324, false, "World"]];
for(item in array) {
  document.write(array[item] + "<br>");
};
//
// array[5][0] = 100;
// alert(array[5].length);
// alert(array.length - 1);


// var arr = ["monday","tuesday","wednesday","thursday","friday"],
//   len = arr.length - 1;
//
// for (var i = 0; i <= len; i++){
//   console.log( "The value of element # " + i + " is: " + arr[i] );
// }
