// var str = "hello, worls, ello";

// alert(array);
// string => array

// var array = str.split(", ");
// for(var i = 0; i < array.length; i++){
//   document.write(array[i] + "<br>");
// }

// array => string

// var arr = ["hello", "worls", "ello"];
// var str = arr.join("; ");
//
// document.write(str);

var arr = ["Hello", "World", "Mango", "World"];
// добавить в конец (213, LAST)
arr.push("Shark", "LAST");
// удалить последнее (LAST)
arr.pop();
// удалить первый элемент (Hello)
arr.shift();
// вставить первый элемент
arr.unshift("FIRST");

for(var i = 0; i < arr.length; i++){
  document.write(arr[i] + "<br>");
}

// Найти индекс (World) => 1
var index = arr.indexOf("World");
document.write(index + "<br>");

// Удалить второй элемент индекса (World)
arr.splice(index, 1);
document.write(arr + "<br>");

// Копировать массив
var newArr = arr.slice();


function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a < b) return -1;
}


var arrNumber = [3, 1, 200, 4, 22, 11, 12];
var value = arrNumber.indexOf(22);
arrNumber[value] = 333;
alert(value);
arrNumber.sort(compareNumeric);

for(var i = 0; i < arrNumber.length; i++){
  document.write(arrNumber[i] + "<br>");
}
