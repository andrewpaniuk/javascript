// Objects
var dog = {
  name : "Snooooopppppy",
  age : "2",
  breed : "Malenkaya",
  color : "Black and White",
  energy : 100,
  eat : function(value){
    dog.energy += value;
  },
  play : function(value){
    dog.energy -= value;
    if(dog.energy <= 0){
      dog.die();
    }
  },
  die : function(){
    alert("DOG IS DEAD.");
  },
  show : function(){
    document.write("<p>" + dog.name + "</p>");
    document.write("<p>" + dog.age + "</p>");
    document.write("<p>" + dog.breed + "</p>");
    document.write("<p>" + dog.color + "</p>");
    document.write("<p style=\"color: red;\">" + dog.energy + "</p>");
    document.write("<hr>");
  },
  dogOne : {
    color : "White"
  }
};

dog.age = 3;

// document.write(dog['name'] + "<br>");
// document.write(dog.age + "<br>");
// document.write(dog.breed + "<br>");
// document.write(dog.color + "<br>");

dog.show();
dog.eat(300);
dog.show();
dog.play(600);


dog.dogOne.color = "Red";
alert(dog.dogOne.color);
