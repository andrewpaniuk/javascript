"use strict";

var boxers = function(id, nick, name, surname, cMatch, cWin, cLoses, cKO){
  var id = id;
  var nick = nick;
  var name = name;
  var surname = surname;
  var cMatch = cMatch || 0;
  var cWin = cWin || 0;
  var cLoses = cLoses || 0;
  var cKO = cKO || 0;
  // set ID
  this.setID = function(id){
    id = id;
    return id;
  };
  // get NICK
  this.getNick = function(){
    return nick;
  };
  // get NAME
  this.getName = function(){
    return name;
  };
  // get SURNAME
  this.getSurname = function(){
    return surname;
  };
  // set COUNTER MATCHES
  this.setcMatch = function(value){
    if (value !== undefined) {
      cMatch = value;
      return cMatch;
    } else {
      return cMatch;
    }
  };
  // set COUNTER WINS
  this.setcWin = function(value){
    if (value !== undefined) {
      cWin = value;
      return cWin;
    } else {
      return cWin;
    }
  };
  this.getcWin = function() {
    return cWin;
  };
  // set COUNTER LOSES
  this.setcLoses = function(value){
    if (value !== undefined) {
      cLoses = value;
      return cLoses;
    } else {
      return cLoses;
    }
  };
  // set COUNTER KO
  this.setcKO = function(value){
    if (value !== undefined) {
      cKO = value;
      return cKO;
    } else {
      return cKO;
    }
  };
};
// boxers list
var mike = new boxers(0, "Iron-Man", "Mike", "Tyson");
var john = new boxers(1, "Hammer", "John", "Poppins");
var harold = new boxers(2, "Harry", "Harold", "Hockin");
var tommy = new boxers(3, "Just a man", "Tommy", "McGray");
var micky = new boxers(4, "Best-of-the-best", "Micky", "Mouse");
var rick = new boxers(5, "Doctor", "Rick", "Madrid");
var robert = new boxers(6, "Stranger", "Robert", "Robertovich");
var tom = new boxers(7, "Enemy", "Tom", "Chase");
var boxersList = [mike, john, harold, tommy, micky, rick, robert, tom];

// boxersList.splice(0, 1);

function tableReset() {
  var table = document.getElementById('tbody');
  var str = "";

  for(var i = 0; i < boxersList.length; i++) {
    str += "<tr>" + "<td>" + boxersList[i].setID(i) + "</td><td>" + boxersList[i].getNick() + "</td><td>"+ boxersList[i].getName() + "</td><td>" + boxersList[i].getSurname() + "</td><td>" + boxersList[i].setcMatch() + "</td><td>" + boxersList[i].setcWin() + "</td><td>" + boxersList[i].setcLoses() + "</td><td>" + boxersList[i].setcKO() + "</tr>";
    table.innerHTML = str;
  }

}

setTimeout(tableReset, 0);

// document.write(mike.setcMatch(mike.setcMatch() + 4));
document.write(boxersList.length + "<br>");
function toss() {
  return Math.round(Math.random() * ((boxersList.length - 1) - 0) + 0);
}
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

// alert(toss());
function round(men) {
  var game = Math.floor(men.length / 2);
  var player1, player2, player1Index, player2Index, winner;
  for (var i = 0; i < game; i++) {
    player1Index = getRandomInt(men.length - 1);
    player1 = men[player1Index];
    men.splice(player1Index, 1);
    player2Index = getRandomInt(men.length - 1);
    player2 = men[player2Index];
    winner = Math.floor(Math.random() * 4);
    men.splice(player2Index, 1);
    // alert(player1.getName());
    // alert(winner + "sd");
    var score1 = player1.setcMatch();
    var score2 = player2.setcMatch();
    document.write(player1.getName() + " VS " + player2.getName() + "<br>");
    if (winner == 0) {
      player1.setcMatch(player1.setcMatch() + 1);
      player1.setcWin(player1.setcWin() + 1);
      document.write("<p style=\"color: red; text-tranform: uppercase;\">" + player1.getName() + " <span style=\"font-weight: 700;\">won</span> </p>");

      player2.setcMatch(player2.setcMatch() + 1);
      player2.setcLoses(player2.setcLoses() + 1);
    } else if (winner == 1) {
      player1.setcWin(player1.setcWin() + 1);
      player1.setcMatch(player1.setcMatch() + 1);
      player1.setcKO(player1.setcKO() + 1);
      document.write("<p style=\"color: red; text-tranform: uppercase;\">" + player1.getName() + " <span style=\"font-weight: 700;\">won with KO</span> </p>");

      player2.setcMatch(player2.setcMatch() + 1);
      player2.setcLoses(player2.setcLoses() + 1);
    } else if (winner == 2) {
      player2.setcWin(player2.setcWin() + 1);
      player2.setcMatch(player2.setcMatch() + 1);
      document.write("<p style=\"color: red; text-tranform: uppercase;\">" + player2.getName() + " <span style=\"font-weight: 700;\">won</span> </p>");

      player1.setcMatch(player1.setcMatch() + 1);
      player1.setcLoses(player1.setcLoses() + 1);
    } else if (winner == 3) {
      player2.setcWin(player2.setcWin() + 1);
      player2.setcMatch(player2.setcMatch() + 1);
      player2.setcKO(player2.setcKO() + 1);
      document.write("<p style=\"color: red; text-tranform: uppercase;\">" + player2 .getName()+ " <span style=\"font-weight: 700;\">won with KO</span> </p>");

      player1.setcMatch(player1.setcMatch() + 1);
      player1.setcLoses(player1.setcLoses() + 1);
    }
  }
}

document.write("<p style=\"font-weight: 700\">ROUND 1</p>");
round(boxersList.slice());
var round2 = [];
for (var i = 0; i < boxersList.length; i++){
    if (boxersList[i].setcWin() == 1){
      round2.push(boxersList[i]);
    }
}
// alert(tom.getcWin());

document.write("<p style=\"font-weight: 700\">ROUND 2</p>");
round(round2.slice());
var final = [];
for (i = 0; i < round2.length; i++){
    if (round2[i].setcWin() == 2){
      final.push(round2[i]);
    }
    // document.write(round2[i].getName() + "<br>");
}
document.write("<p style=\"font-weight: 700\">ROUND 3</p>");
round(final.slice());
// var final = [];
for (i = 0; i < final.length; i++){
    if (final[i].setcWin() == 3){
      document.write(final[i].getName() + " Winner of the FINAL <br>");
    } else {
      // document.write(final[i].getName() + "<br>");
    }
}



// window.onload = function() {
// var select1 = document.getElementById('select1');
// var select2 = document.getElementById('select2');
// var options1 = "";


// for(var i = 0; i < boxersList.length; i++) {
//
//   options1 += "<option value=\"" + boxersList[i].setID(i) + "\">" + boxersList[i].getNick() + "</option>";
//
//
//
// }
// select1.innerHTML = options1;
// select2.innerHTML = options1;
// var selected1 = select1.options[select1.selectedIndex].value;
// var selected2 = select2.options[select2.selectedIndex].value;


// function show() {
//   var textHtml = document.getElementById('txt');
//   textHtml.innerHTML = selected1 + " " + selected2;
// }
// setTimeout(show, 500);
// };



// var tossResult = toss();
// alert(tossResult);
//
// function tours() {
//   var firstMatchHtml = document.getElementById('FirstMatch');
//   var vs = [];
//   while(true) {
//
//     var toss1 = toss();
//     var toss2 = toss();
//
//     if(boxersList[toss1] != boxersList[toss2]) {
//       vs.unshift(boxersList[toss1].getName(), boxersList[toss2].getName());
//       // alert(vs[0] + " VS " + vs[1]);
//       break;
//       // if(vs.length == 8) {
//       //   break;
//       // }
//     }
//
//   }
//   this.getVS = function() {
//     return vs;
//   };
//
//   firstMatchHtml.innerHTML = vs.join(" VS ");
//
// }
//
// tours();

// function tossFight() {
//   var shake = Math.round(Math.random() * ((4 - 1) - 0) + 0);
//   return shake;
// }
// //
// // alert(tossFight());
//
// function fight() {
//   var vs = getVS();
//   var winner = '';
//   while(true) {
//     var score1 = tossFight();
//     var score2 = tossFight();
//     // alert(score1 + " " + score2);
//     if(score1 == 3 && score2 != 3) {
//       alert(vs[0] + " winner with KO");
//       winner = vs[0];
//       break;
//     } else if(score2 == 3 && score1 != 3) {
//       alert(vs[1] + " winner with KO");
//       winner = vs[1];
//       break;
//     } else if(score1 > score2) {
//         alert(vs[0] + " winner");
//         winner = vs[0];
//         break;
//     } else if(score2 > score1) {
//         alert(vs[1] + " winner");
//         winner = vs[1];
//         break;
//     }
//   }
//   return winner;
// }
// fight();
// var winner = fight();
// alert(winner + " YOUUU are WINNER");
