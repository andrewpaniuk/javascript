var boxers = function(sn, nm,nk, countMatch, countVictory, vicKnockout, countLoss, draw, id){
    var surname = sn;
    var name = nm;
    var nick = nk;
    this.countMatch = countMatch || 0;
    this.countVictory = countVictory || 0;
    this.vicKnockout = vicKnockout || 0;
    this.countLoss = countLoss || 0;
    this.draw = draw || 0;
    var ident = id;
    this.showDetails = function () {
        return ident + ". " + surname + "&nbsp;" + name + " (" + nick + ") : " + "match: " + this.countMatch  + ", " +
            "victory:  " + this.countVictory + ", " + "knockout: " + this.vicKnockout + ", " + "loss: " + this.countLoss + ", " +
            "draw: " + this.draw + ".";
    };
    this.getSurname = function() {
        return sn;
    };
    this.getName = function() {
        return nm;
    };
    this.getNick = function() {
        return nk;
    };
    this.getcountMatch = function(){
       return this.countMatch;
  };
    this.setcountMatch = function(countMatch){
    if (typeof countMatch === "number") {
      this.countMatch = countMatch;
      return this.countMatch;
    }
  };
   this.getCountVictory = function() {
       return this.countVictory;
   };
   this.setCountVictory = function(countVictory) {
       if (typeof countVictory === "number"){
         this.countVictory = countVictory;
         return this.countVictory;
       }
   };
    this.getVictoryKnockout = function() {
        return this.vicKnockout;
    };
    this.setVictoryKnockout = function(vicKnockout) {
        if (typeof (vicKnockout) === "number"){
            this.vicKnockout = vicKnockout;
            return this.vicKnockout;
        }
    };
    this.getCountLoss = function() {
        return this.countLoss;
    };
    this.setCountLoss = function(countLoss) {
        if (typeof countLoss === "number"){
            this.countLoss = countLoss;
            return this.countLoss;
        }
    };
    this.getCountDraw = function() {
        return this.draw;
    };
    this.setCountDraw = function(draw) {
        if (typeof draw === "number"){
            this.draw = draw;
            return this.draw;
        }
    };
    this.getId = function () {
        return ident;
    };
};
var Muhammad = new boxers("Muhammad", "Ali", "AliGreat", 0, 0, 0, 0, 0, 1);
var Sugar = new  boxers("Robinson", "Ray", "Sugar", 0, 0, 0, 0, 0, 2);
var Mike = new  boxers("Tyson", "Mike", "Greatmike", 0, 0, 0, 0, 0, 3);
var Rocky = new boxers("Marciano", "Rocky", "RockyOne", 0, 0, 0, 0, 0, 4);
var Louis = new  boxers("Louis", "Joe", "Loi", 0, 0, 0, 0, 0, 5);
var Smokin = new boxers("Frazier","Joe", "Smokin", 0, 0, 0, 0, 0, 6);
var Samaniego = new boxers("Duran", "Roberto", "Samaniego",0, 0, 0, 0, 0, 7);
var Thomas = new  boxers("Hearns", "Thomas", "Hearn", 0, 0, 0, 0, 0, 8);
var boxersArray = [Muhammad, Sugar, Mike, Rocky, Louis, Smokin, Samaniego, Thomas];
var i;
for (i = 0; i < boxersArray.length; i++){
  document.write(boxersArray[i].showDetails() + "<br>");
}
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
function fight() {
    return getRandomInt(4);
}
function round(men) {
  var games = Math.floor(men.length/2);
  var player1, player2, player1Index, player2Index, winner;
  for (var i = 0; i < games; i++) {
      player1Index = getRandomInt(men.length);
      player1 = men[player1Index];
      men.splice(player1Index, 1);
      player2Index = getRandomInt(men.length);
      player2 = men[player2Index];
      men.splice(player2Index, 1);
      winner = fight();
      alert(winner);
        if (winner == 0) {
          player1.countVictory+=1;
          player1.countMatch+=1;

          player2.countMatch+=1;
          player2.countLoss+=1;
        } else if (winner == 1) {
          player1.countVictory+=1;
          player1.countMatch+=1;
          player1.vicKnockout+=1;

          player2.countMatch+=1;
          player2.countLoss+=1;
        } else if (winner == 2) {
          player2.countVictory+=1;
          player2.countMatch+=1;

          player1.countMatch+=1;
          player1.countLoss+=1;
        } else if (winner == 3) {
          player2.countVictory+=1;
          player2.countMatch+=1;
          player2.vicKnockout+=1;

          player1.countMatch+=1;
          player1.countLoss+=1;
        }
      }
}

document.write("<br>" + "Round 1:" + "<br>");
round(boxersArray.slice());
var round2 = [];
for (i = 0; i < boxersArray.length; i++){
    if (boxersArray[i].getCountVictory() == 1){
      round2.push(boxersArray[i]);
    }
    document.write(boxersArray[i].showDetails() + "<br>");
}
document.write("<br>" + "Round 2:" + "<br>");
round(round2.slice());
var round3 = [];
for (i = 0; i < round2.length; i++){
  if (round2[i].getCountVictory() == 2){
    round3.push(round2[i]);
  }
  document.write(round2[i].showDetails() + "<br>");
}
document.write("<br>" + "Round 3:" + "<br>");
round(round3.slice(0));
for (i = 0; i < round3.length; i++){
    if (round3[i].getCountVictory() == 3) {
        document.write(round3[i].showDetails() + " is the winner!!! <br>");
    }else {
        document.write(round3[i].showDetails() + "<br>");
    }
}
