'use strict';

// var dog = function(name, age) {
//   this.name = name;
//   this.age = age;
//   this.barking = function(){
//     document.write(this.name + ' is barking....' + '<br>');
//   };
// };
//
// var rex = new dog('Rex', 20);
// var pes = new dog('Pes', 20);
//
// var dogsList = [rex, pes];
//
// rex.barking();
//
// var test = rex.barking;


// PROTOTYPE __PROTO__
//
// PROTOTYPE CONSTRUCTOR
// var dogFight = function() {
//   this.fight = function() {
//     alert(this.name + ' is fighting');
//   };
// };
//
// rex.__proto__ = new dogFight();
// rex.fight();


// OBJECT
// var objectDog = {
//   dFight : function() {
//     alert('qwe');
//   }
// };
// rex.__proto__ = objectDog;
// rex.dFight();
//

// dog.prototype.canFight = function() {
//   alert(this.name + ' is fighting');
// };
//
// dog.prototype.canHunt = function() {
//   alert(this.name + ' is hunting');
// };
//
// rex.canFight();
// pes.canHunt();


// =======================================================





var person = {
  constructor : function(name, surname, age, gender) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.gender = gender;
    return this;
  },
  welcome : function() {
    if (this.age > 70) {
      document.write('Welcome, ' + '<strong>' + this.name + ' ' + this.surname + '</strong>' + '! You are ' + this.age + ' year old. OMG, you are older than our Earth. Go and relax!<br>');
    } else {
      document.write('Welcome, ' + '<strong>' + this.name + ' ' + this.surname + '</strong>' + '! You are ' + this.age + ' year old.<br>');
    }
  }
};

// var jimmy = person.constructor('Jimmy', 'Hook', 20, 'MAN');
// jimmy.welcome();

var student = Object.create(person);

student.constructor = function(name, surname, age, gender, subjects) {
  person.constructor.apply(this, arguments);
  this.subjects = subjects;
  this.study = function() {
    document.write('Study... <br>');
  };
  return this;
};

var tommy = Object.create(student).constructor('Tommy', 'Smith', 15, 'MAN', ['HTML', 'CSS', 'JS']);
// var tommy = student.constructor('Tommy', 'Smith', 15, 'MAN', ['HTML', 'CSS', 'JS']);

tommy.welcome();
tommy.study();
document.write(tommy.subjects + '<br>');
document.write(person.isPrototypeOf(tommy) + '<br><br><br>');


var programmer = Object.create(student);

programmer.constructor = function(name, surname, age, gender, subjects, coding) {
  student.constructor.apply(this, arguments);
  this.coding = coding;
  this.canCode = function() {
    if (typeof(this.subjects) == 'array') {
      this.subjects.join('; ');
    }
    document.write(this.name + ' is coding now! You know ' + this.subjects + '<br>');
  };
  return this;
};

var rick = Object.create(programmer).constructor('Rick', 'Rickman', 56, 'Real man', ['c++', 'c#'], 'Everything');
rick.welcome();
rick.canCode();
document.write(student.isPrototypeOf(programmer) + '<br><br><br>');


var designs = Object.create(student);

designs.constructor = function(name, surname, age, gender, subjects, draw) {
  student.constructor.apply(this, arguments);
  this.draw = draw;
  this.canDraw = function() {
    document.write(this.name + ' is drawing now! <br>');
  };
  return this;
};

var valera = Object.create(designs).constructor('Valera', 'Karpovich', 78, 'Big boy', ['Photoshop', 'Illustrator'], 'Picture');

valera.welcome();
valera.canDraw();
document.write(student.isPrototypeOf(designs) + '<br><br><br>');
