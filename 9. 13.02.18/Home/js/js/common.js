'use strict';

var transport = {
  constructor : function(manufucture, model, series, color) {
    this.manufucture = manufucture;
    this.model = model;
    this.color = color;
    this.series = series;
    return this;
  },
  showInfo : function() {
    document.write(this.manufucture + ' ' + this.model + ' ' + this.series + ' ' + this.color + '<br>');
  }
};

var transportCars = Object.create(transport);
transportCars.constructor = function(manufucture, model, series, color, bodyStyle, doors) {
  transport.constructor.apply(this, arguments);
  this.bodyStyle = bodyStyle;
  this.doors = doors;
  this.showInfoCars = function() {
    document.write(this.manufucture + ' has ' + this.doors + ' doors. Style body: ' + this.bodyStyle + '.<br><br>');
  };
  return this;
};

var transportAir = Object.create(transport);
transportAir.constructor = function(manufucture, model, series, color, produced, firstFlight) {
  transport.constructor.apply(this, arguments);
  this.produced = produced;
  this.firstFlight = firstFlight;
  this.showInfoAir = function() {
    document.write(this.manufucture + ' produced ' + this.produced + '. First flight: ' + this.firstFlight + '.<br><br>');
  };
  return this;
};

var transportRail = Object.create(transport);

transportRail.constructor = function(manufucture, model, series, color, constructed, engineType, weight) {
  transport.constructor.apply(this, arguments);
  this.constructed = constructed;
  this.engineType = engineType;
  this.weight = weight;
  this.showInfoRail = function() {
    document.write(this.manufucture + ' constructed ' + this.constructed + '. Engine type: ' + this.engineType + '. Weight: ' + this.weight + '.<br><br>');
  };
  return this;
};


document.write('<strong>Road transport</strong><br>');
var VW_B6 = Object.create(transportCars).constructor('Volkswagen ', 'Passat', 'B6', 'Black', 'Sedan', 4);
VW_B6.showInfo();
VW_B6.showInfoCars();

document.write('<strong>Air transport</strong><br>');
var boeing_474 = Object.create(transportAir).constructor('Boeing', '747', '', 'White', '1968–present', 'February 9, 1969');
boeing_474.showInfo();
boeing_474.showInfoAir();

var hindenburg_LZ129 = Object.create(transportAir).constructor('Hindenburg', 'LZ129', '', 'White', '1936–37', 'March 4, 1936');
hindenburg_LZ129.showInfo();
hindenburg_LZ129.showInfoAir();



document.write('<strong>Rail transport</strong><br>');
var regioSwinger_HZ7123 = Object.create(transportRail).constructor('RegioSwinger', 'HZ 7123', '', 'White', '1998-present', 'Diesel', '116 t (114 long tons; 128 short tons)');
regioSwinger_HZ7123.showInfo();
regioSwinger_HZ7123.showInfoRail();
// alert(transport.isPrototypeOf(transportAir));
