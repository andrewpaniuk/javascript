﻿#ES5 🔥🔥🔥
## 1.16.01.18
variables: var
## 2.18.01.18
if...else, switch...case
## 3.23.01.18
while, for, do...while
## 4.25.01.18
function
## 5.30.01.18
recursive function, function callback, array, for...in
## 6.01.02.18
js: array, methods array | js2: object
## 7.06.02.18
JSON
## 8.08.02.18
constructor (box)
## 10.15.02.18
DOM, (radio, selected)
## 11.20.02.18
registration
## 12.22.02.18
addEventListener
## 13.27.02.18
addEventListener handler
## 14.01.03.18
Star Wars (swapi.co)
## 15.15.03.18
Date
# ES6 🔥🔥🔥
## 16.15.03.18
variables: const, let | localStorage, sessionStorage, cookies | webpack
## 17.22.03.18
arrow function | calc
## 18.27.03.18
destructuring array and object
## 19.29.03.18
class | get, set
## 20.03.04.18
modules webpack
## 21.05.04.18
classwork show post JSON
## 22.17.04.18
promise
## 23.19.04.18
promise 2 (movies posters)
## 24.24.04.18 (es5) 🔥
patterns (app)
## 25.26.04.18 (es5) 🔥
patterns 2
## 26.01.05.18 (es5) 🔥
patterns 2 (iteration, strategy)
## 27.03.05.18 (es5) 🔥
class: patterns 2 (proxy object) | home: observe
## 28.08.05.18 (es5) 🔥
jquery
## 29.10.05.18 (es5) 🔥
jquery inputs
